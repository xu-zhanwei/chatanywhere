import { createApp } from "vue";
import "@/styles/reset.css";
import "@/styles/font.css";
import "@/styles/animate.css";
import "@/styles/iconfont.css";
import { Site } from "./config/base";
import App from "@/App.vue";
import router from "./router";
import { VueMasonryPlugin } from "vue-masonry";
import mitt from "mitt";

// 额外引入图标库
import ArcoVueIcon from "@arco-design/web-vue/es/icon";
import "@arco-design/web-vue/dist/arco.css";
import "@arco-themes/vue-chatanywhere/theme.css";

// 引入兔小巢
import TxcChangeLog from "txc-change-log";

new TxcChangeLog({ id: Site.txcId }).activateChangeLog(); // 更新日志插件

// # highlight 的样式，依赖包，组件
import "highlight.js/styles/atom-one-dark.css"; //
import "highlight.js/lib/common";
import hljsVuePlugin from "@highlightjs/vue-plugin";

// 实时检测 web 应用更新的 JavaScript 库
import { createVersionPolling } from "version-polling";
import { hope } from "./tools/hope/@types/hope";
import registerComponent from "@/tools/hope/template/index";

const app = createApp(App);
registerComponent(app);

// 引入代码高亮，并进行全局注册
app.use(hljsVuePlugin);

app.config.globalProperties.emitter = mitt();
app.use(ArcoVueIcon);
app.use(router);
app.use(VueMasonryPlugin);
app.mount("#app");

createVersionPolling({
  appETagKey: "zaiwen",
  pollingInterval: 5 * 60 * 1000, // 单位为毫秒
  silent: process.env.NODE_ENV === "development", // 开发环境下不检测
  onUpdate: (self) => {
    // 当检测到有新版本时，执行的回调函数，可以在这里提示用户刷新页面
    hope.updatePopup({ cancel: self.onCancel, confirm: self.onRefresh });
  },
});

if (process.env.NODE_ENV === "production") {
  console.log = () => {};
  console.warn = () => {};
  console.clear();
}
