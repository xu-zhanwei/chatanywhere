/** 一天时间戳 */
export const oneDayTimestamp = 86400000;
/** 一小时 */
export const oneHourTimestamp = 3600000;
