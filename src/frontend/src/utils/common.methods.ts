/** uuid */
export function getUuid(len: number = 17) {
  return "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
    .replace(/[xy]/g, function (c) {
      const r = (Math.random() * 16) | 0,
        v = c == "x" ? r : (r & 0x3) | 0x8;
      return v.toString(16);
    })
    .slice(0, len);
}

/**
 * 文件流 转 Base64
 * @param file
 * @returns
 */
export function fileToBase64(file: File): Promise<string> {
  return new Promise((resolve) => {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = async (e: any) => {
      resolve(String(e.target.result));
    };
  });
}
/**
 * 文件流 转 Blob
 * @param file
 * @returns
 */
export function fileToBlob(file: File): Blob {
  return new Blob([file], { type: file.type });
}
/**
 * Blob 转 文件流
 * @param blob
 * @returns
 */
export function blobToFile(blob, fileName = getUuid()): File {
  const fileType = blob.type;
  const newFile = new File([blob], fileName, { type: fileType });
  return newFile;
}

/** 格式化时间  去除时间戳的时分秒 */
export function formatDateHMS(timeStamp: string | number): number {
  const stamp = String(timeStamp).length === 11 ? Number(timeStamp) * 1000 : timeStamp;
  const date = timestampToTime(stamp, "/");
  return new Date(date).getTime();
}

export const weekName: string[] = ["日", "一", "二", "三", "四", "五", "六"];
export function getWeekName(index: number, prefix: string = "周"): string {
  return prefix + weekName[index];
}
export function timestampToWeek(timeStamp: string | number, prefix: string = "周"): string {
  const stamp = String(timeStamp).length === 13 ? Number(timeStamp) : Number(timeStamp) * 1000;
  const day = new Date(stamp).getDay();
  return getWeekName(day, prefix);
}

/** FormatDate  */
export function FormatDate(timeStamp: string | number, fmt: string = "yyyy-MM-dd hh:mm:ss") {
  const date = new Date(timeStamp);
  var o: any = {
    "M+": date.getMonth() + 1, // 月份
    "d+": date.getDate(), // 日
    "h+": date.getHours(), // 小时
    "m+": date.getMinutes(), // 分
    "s+": date.getSeconds(), // 秒
    "q+": Math.floor((date.getMonth() + 3) / 3), // 季度
    S: date.getMilliseconds(), // 毫秒
  };
  if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
  for (var k in o) {
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, RegExp.$1.length === 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
  }

  return fmt;
}

/** 写入网页标题 */
export function setDocumentTitle(title: string): void {
  document.title = title;
}

/** 按指定格式格式化时间 yyyy-MM-dd hh:mm:ss */
export function formatTime(date: Date, format: string = "yyyy-MM-dd hh:mm:ss"): string {
  var o = {
    "M+": date.getMonth() + 1, //月份
    "d+": date.getDate(), //日
    "h+": date.getHours(), //小时
    "m+": date.getMinutes(), //分
    "s+": date.getSeconds(), //秒
  };
  if (/(y+)/.test(format)) {
    //根据y的长度来截取年
    format = format.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
  }
  for (var k in o) {
    if (new RegExp("(" + k + ")").test(format)) format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
  }
  return format;
}

/**
 * 格式化时间戳
 * @param timeStamp 时间戳
 * @param connector zh为中文，默认为 / ，可自定义传入连接符
 * @returns
 */
export function timestampToTime(timeStamp: string | number, connector: string = "/"): string {
  const stamp = String(timeStamp).length < 13 ? Number(timeStamp) * 1000 : timeStamp;
  const date = new Date(stamp);
  if (connector === "zh") {
    const Y = date.getFullYear() + "年";
    const M = (date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1) + "月";
    const D = date.getDate() + "日";
    return Y + M + D;
  } else {
    const Y = date.getFullYear() + connector;
    const M = (date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1) + connector;
    const D = date.getDate();
    return Y + M + D;
  }
}

/**
 * 图片地址转 Base64
 * @param url
 * @param outputFormat
 * @returns
 */
export function imageUrlToBase64(url: string, outputFormat: string = "image/jpg"): Promise<string> {
  return new Promise<string>((resolve, reject) => {
    var canvas: any = document.createElement("canvas");
    const ctx: any = canvas.getContext("2d");
    const img = new Image();
    img.crossOrigin = "Anonymous";
    img.src = url;
    console.log(canvas);
    img.onload = function () {
      canvas.height = img.height;
      canvas.width = img.width;
      ctx.drawImage(img, 0, 0);
      var dataURL = canvas.toDataURL(outputFormat);
      canvas = null;
      resolve(dataURL);
    };
    img.onerror = (err) => {
      reject(err);
    };
  });
}

/**
 * Base64 转 文件流
 * @param dataurl
 * @param filename
 * @returns
 */
export function base64toFile(dataurl: string, filename: string = "file"): File {
  const arr: any = dataurl.split(",");
  const mime = arr[0].match(/:(.*?);/)[1];
  const suffix = mime.split("/")[1];
  const bstr = atob(arr[1]);
  let n = bstr.length;
  const u8arr = new Uint8Array(n);
  while (n--) {
    u8arr[n] = bstr.charCodeAt(n);
  }
  return new File([u8arr], `${filename}.${suffix}`, {
    type: mime,
  });
}

// 获取assets静态资源
export function getImageUrl(url: string): string {
  return new URL(url.toString(), import.meta.url).href;
}

/**
 * 浏览器url参数转json
 * @returns
 */
export function searchToJson<T>(param: string): T {
  var newObj: any = new Object() as T;
  var strs = param.split("&");
  for (var i = 0; i < strs.length; i++) {
    newObj[strs[i].split("=")[0]] = strs[i].split("=")[1] || "";
  }
  return newObj;
}

/** json转浏览器url参数 */
export function jsonToParams(params) {
  const urlParams = new URLSearchParams();
  for (const key in params) {
    urlParams.append(key, params[key]);
  }
  return urlParams.toString();
}

/**
 * 按指定关键字段 去重数组
 * @param arr
 * @param key
 * @returns
 */
export function deduplication<T>(arr: T[], key: string = "uid"): T[] {
  let obj: any = {};
  return arr.filter((val: any) => (obj[val[key]] ? "" : (obj[val[key]] = true)));
}

/** 校验手机号码 */
export function regExpMobil(value: string): boolean {
  const regExp = new RegExp("^1[3578]\\d{9}$");
  return regExp.test(value);
}

/** 校验邮箱 */
export function regExpEmail(value: string): boolean {
  const regExp = new RegExp("^[a-zA-Z0-9]+([-_.][A-Za-zd]+)*@([a-zA-Z0-9]+[-.])+[A-Za-zd]{2,5}$");
  return regExp.test(value);
}

/** 校验密码强度  必须包含数字、英文字母、特殊符号且大于等于6位 */
export function regExpPassword(value: string): boolean {
  var regExp = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[`~!@#$%^&*()_+<>?:"{},.\/\\;'[\]])[A-Za-z\d`~!@#$%^&*()_+<>?:"{},.\/\\;'[\]]{6,}$/;
  return regExp.test(value);
}

/** 校验 传入参数是否为图片链接 */
export function regExpImage(src: string): Promise<boolean> {
  return new Promise((resolve) => {
    var ImgObj = new Image();
    ImgObj.src = src;
    ImgObj.onload = () => resolve(true);
    ImgObj.onerror = () => resolve(false);
  });
}

/** 校验 传入地址链接可访问性 */
export function regURL(url: string): boolean {
  try {
    new URL(url);
    return true;
  } catch (error) {
    return false;
  }
}

/** 写入粘贴板 */
export function clipboardWriteText(text: string): Promise<boolean> {
  return new Promise<boolean>((resolve) => {
    try {
      const element = document.createElement("textarea");
      element.value = text;
      element.setAttribute("readonly", "");
      element.style.position = "absolute";
      element.style.left = "-9999px";
      document.body.appendChild(element);
      element.select();
      document.execCommand("copy");
      document.body.removeChild(element);
      resolve(true);
    } catch (error) {
      resolve(false);
    }
  });
}

/**
 * 将svg导出成图片
 * @param node svg节点 => document.querySelector('svg')
 * @param name 生成的图片名称
 * @param width 生成的图片宽度
 * @param height 生成的图片高度
 * @param type 生成的图片类型
 */
export function covertSVG2Image(node: Element, name: string, width: number, height: number, type = "png"): Promise<string> {
  return new Promise((resolve, reject) => {
    try {
      let serializer = new XMLSerializer();
      let source = '<?xml version="1.0" standalone="no"?>\r' + serializer.serializeToString(node);
      let image = new Image();
      image.src = "data:image/svg+xml;charset=utf-8," + encodeURIComponent(source);
      let canvas = document.createElement("canvas");
      canvas.width = width;
      canvas.height = height;
      let context = canvas.getContext("2d");
      context.fillStyle = "#fff";
      context.fillRect(0, 0, 2000, 2000);
      image.onload = function () {
        context.drawImage(image, 0, 0);
        // 将canvas转换为Blob对象
        canvas.toBlob(function (blob) {
          if (!isWeChatBrowser()) {
            // 创建下载链接
            let a = document.createElement("a");
            a.download = `${name}.${type}`;
            a.href = URL.createObjectURL(blob);
            // 触发下载
            a.click();
            // 清理资源
            URL.revokeObjectURL(a.href);
          }
        }, `image/${type}`);
        const base64 = canvas.toDataURL();
        resolve(base64);
      };
    } catch (error) {
      reject(error);
    }
  });
}

// 下载txt文件
export function downloadTxtFile(content, filename) {
  // 创建一个Blob对象，指定文本和数据类型
  const blob = new Blob([content], { type: "text/plain" });
  // 创建一个下载链接
  const downloadLink = document.createElement("a");
  downloadLink.href = URL.createObjectURL(blob);
  downloadLink.download = filename;
  // 模拟点击下载链接
  downloadLink.click();
}

/** 检查用户是否在微信内置浏览器中 */
export function isWeChatBrowser() {
  // 获取用户代理字符串
  var userAgent = navigator.userAgent.toLowerCase();
  // 检查用户代理字符串中是否包含"WeChat"
  if (userAgent.indexOf("micromessenger") >= 0 || userAgent.indexOf("wechatdevtools") >= 0) {
    return true; // 在微信内置浏览器中
  } else {
    return false; // 不在微信内置浏览器中
  }
}

/** 使用浏览器内置功能将文字转换为语音 */
export function speechSynthesis(text: string) {
  const synthesis = window.speechSynthesis;
  const utterance = new SpeechSynthesisUtterance(text);

  synthesis.speak(utterance);
}

/** 获取浏览器以使用的localStorage存储大小 */
export function getLocalStorageUseSize() {
  var size = 0;
  for (const key in window.localStorage) {
    if (window.localStorage.hasOwnProperty(key)) {
      size += window.localStorage.getItem(key).length;
    }
  }
  return (size / 1024 / 1024).toFixed(2) + "MB";
}

/** 获取图片信息 */
export function getImageEl(src: string): Promise<HTMLImageElement> {
  // console.log("调用了  getImageEl ", src);
  return new Promise((resolve) => {
    // 创建一个新的图片对象
    const img = new Image();
    // 监听图片加载完成事件
    img.onload = function () {
      resolve(img);
    };
    img.src = src;
  });
}

/** 去除对象中值为： ""/0/null/undefined/{}/[]   的键值对， 不会深层遍历 */
export function paramHandler(params: object): object {
  if (typeof params == "object") {
    for (let key of Object.keys(params)) {
      if (!params[key] || params[key] == null || params[key] == undefined) {
        delete params[key];
      } else if (params[key] instanceof Array) {
        const arr = [];
        const list = params[key];
        for (let listElement of list) {
          const abj = paramHandler(listElement);
          abj && arr.push(abj);
        }
        if (arr.length > 0) {
          params[key] = arr;
        } else {
          delete params[key];
        }
      } else if (typeof params[key] === "object" && Object.keys(params[key]).length === 0) {
        delete params[key];
      }
    }
  }
  return params;
}
