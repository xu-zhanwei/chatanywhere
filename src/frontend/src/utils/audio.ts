/** 统一语音管理器 */
export class AudioContext {
  private static _ins: AudioContext = new AudioContext();


  constructor() {

  }

  public static get ins(): AudioContext {
    return this._ins ? this._ins : (this._ins = new AudioContext());
  }
}