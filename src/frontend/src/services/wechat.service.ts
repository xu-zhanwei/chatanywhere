
import { appContext } from "@/models/app.context";
import { BaseServices } from "./base.services";


export class WeChatService extends BaseServices {
  private static _ins: WeChatService = new WeChatService();
  public static get ins(): WeChatService {
    return this._ins ? this._ins : (this._ins = new WeChatService());
  }

  /** 获取微信 jssdk 签名 */
  public signature(): Promise<void> {

    const url = "https://jump.zaiwen.top/wechat/signature";
    return new Promise<void>((resolve) => {
      this.get(url).then((res: any) => {
        const { timestamp, nonceStr, signature } = res?.info;
        // @ts-ignore
        wx.config({
          debug: true, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
          appId: 'wx6e303e7c2a2b23a6', // 必填，公众号的唯一标识
          timestamp, // 必填，生成签名的时间戳
          nonceStr, // 必填，生成签名的随机串
          signature,// 必填，签名
          jsApiList: ['miniProgram'] // 必填，需要使用的JS接口列表
        });

        // @ts-ignore
        wx.miniProgram.postMessage({
          data: {
            key: 'value'
          }
        })
        resolve();
      });
    });
  }

}


export const weChatService: WeChatService = WeChatService.ins;
