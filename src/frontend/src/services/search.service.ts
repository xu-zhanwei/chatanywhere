/*
 * @Author: zhanwei xu
 * @Date: 2023-08-19 17:30:45
 * @LastEditors: zhanwei xu
 * @LastEditTime: 2023-10-02 09:07:08
 * @Description: 
 * 
 * Copyright (c) 2023 by zhanwei xu, Tsinghua University, All Rights Reserved. 
 */
import { appRef } from "@/models/app.ref";
import { BaseServices } from "./base.services";
export class SearchService extends BaseServices {
  private static _ins: SearchService = new SearchService();
  public static get ins(): SearchService {
    return this._ins ? this._ins : (this._ins = new SearchService());
  }

  public search(key: string, message: string, chunk: (data: string) => void, end: (data: string) => void): void {
    const url = "https://www.gaosijiaoyu.cn/search";
    const data = { message };
    return this.stream(key, url, data, chunk, end);
  }

  public message(key: string, content: string, chunk: (data: string) => void, end: (data: string) => void): void {
    const message = [{ role: "user", content }];
    const url = "https://bak.cwjiaoyu.cn/message";
    const data = { message, user_key: appRef.user.data.openai_key };
    return this.stream(key, url, data, chunk, end);
  }
}

export const searchService: SearchService = SearchService.ins;
