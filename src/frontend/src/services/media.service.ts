import { GalleryImageInfo } from "@/models/media";
import { BaseServices } from "./base.services";
import { RoleEnum } from "@/models/@types";
import { Method, RespInfo, Uri, config } from "./config";
import { appRef } from "@/models/app.ref";
import { AssetInfo, AssetStatus } from "@/models/asset";
import { Message } from "@arco-design/web-vue";

export type GeneratePromptReq = {
  role: RoleEnum;
  content: string;
};

export class MediaService extends BaseServices {
  private static _ins: MediaService = new MediaService();
  public static get ins(): MediaService {
    return this._ins ? this._ins : (this._ins = new MediaService());
  }

  public list(page: number = 1, number: number = 100): Promise<GalleryImageInfo[]> {
    const url = config.httpHost + Uri.Media + Uri.Image + Method.List;
    const data = { page, number };
    return new Promise<GalleryImageInfo[]>((resolve) => {
      const list = appRef.media.gallery.list(page, number);
      console.log(list);
      if (list?.length) {
        resolve(list);
        const data = { page: ++page, number };
        this.request(url, data, "GET").then((res: RespInfo<GalleryImageInfo>) => {
          res.list?.map(async (val) => {
            const src = res.baseurl + val.image_id + ".webp";
            new Image().src = src;
          });
          appRef.media.gallery.setList(page, number, res.list);
        });
      } else {
        this.request(url, data, "GET").then((res: RespInfo<GalleryImageInfo>) => {
          appRef.media.gallery.setList(page, number, res.list);
          appRef.media.gallery.baseurl = res.baseurl;
          appRef.media.gallery.total = res.total;
          resolve(res.list);
          this.list(page, number);
        });
      }
    });
  }

  public getOne(): Promise<GalleryImageInfo> {
    const url = config.httpHost + Uri.Media + Uri.Image + Method.List;
    const data = { page: 1, number: 1 };
    return new Promise<GalleryImageInfo>((resolve) => {
      this.request(url, data, "GET").then(async (res: RespInfo<GalleryImageInfo>) => {
        const image = res.list[0];
        // await fetch(res.baseurl + image.image_id + ".webp")
        //   .then((response) => response.blob())
        //   .then((blob: Blob) => {
        //     image.blob = blob;
        resolve(image);
        //   });
      });
    });
  }

  /** 创建一个图像至画廊 */
  public add(asset: string, prompt: string, negativePrompt?: string): Promise<void> {
    const user = appRef.user.data.uid;
    const url = config.httpHost + Uri.Gallery + Uri.Image + Method.Add;
    const data = { user, asset, prompt, negativePrompt };
    return new Promise<void>((resolve) => {
      this.request(url, data).then(async () => {
        await appRef.aidraw.share();
        Message.success("分享成功")
        resolve();
      });
    });
  }

  /** 获取全部图像、包含未审核 */
  public all(page: number, number: number, keyword?: string, status?: AssetStatus): Promise<GalleryImageInfo[]> {
    const url = config.httpHost + Uri.Gallery + Uri.Image + Method.List;
    const data = { page, number, keyword, status };
    return new Promise<GalleryImageInfo[]>((resolve) => {
      this.request(url, data, "GET").then((res: RespInfo<GalleryImageInfo>) => {
        resolve(res.list);
      });
    });
  }
}

export const mediaService: MediaService = MediaService.ins;
