import { BaseServices } from "./base.services";
import { Method, RespInfo, Uri, config } from "./config";
import { appContext } from "@/models/app.context";
import { LoginType, SexEnum } from "@/models/@types";
import { Md5 } from "ts-md5";
import { UserInfo } from "@/models/user";
import { NotifyOption, NotifyType, appNotify } from "@/models/app.notify";
import { hope } from "@/tools/hope/@types/hope";
import { WxOptionType } from "@/models/wx";
import { appRef } from "@/models/app.ref";
import { Message } from "@arco-design/web-vue";
import { synchronousService } from "./synchronous.service";

class UserService extends BaseServices {
  constructor() {
    super();
  }

  /** 手机号验证码 / 手机号密码 登录 （Md5 加密） */
  public login(type: LoginType = LoginType.Phone, code: string, phone?: string): Promise<void> {
    if (type === LoginType.Password) code = Md5.hashStr(code);
    const url = config.usersbeasking + Uri.User + Method.Login;
    const data = { type, code, phone };
    return new Promise((resolve) => {
      return this.request(url, data).then(async (res: RespInfo) => {
        appContext.setGlobal({ token: res.token });
        await synchronousService.down();
        location.reload();
        // await this.token();
        resolve();
      });
    });
  }

  /** 注册 */
  public register(code: string, password?: string, invitation_code?: string): Promise<void> {
    const url = config.usersbeasking + Uri.User + Method.Register;
    const data = { code, password: Md5.hashStr(password), invitation_code };
    return new Promise((resolve) => {
      return this.request(url, data).then(async (res: RespInfo) => {
        appContext.setGlobal({ token: res.token });
        await synchronousService.allUp();
        location.reload();
        resolve();
      });
    });
  }

  /** 重置密码 */
  public reset(code: string, password?: string): Promise<void> {
    const url = config.usersbeasking + Uri.User + Method.Password;
    const data = { code, password: Md5.hashStr(password) };
    return new Promise((resolve) => {
      return this.request(url, data).then(async () => {
        Message.success("密码修改成功");
        resolve();
      });
    });
  }

  public update(nick?: string, avatar?: string, sex?: SexEnum, birthday?: number, openid?: string): Promise<void> {
    const url = config.usersbeasking + Uri.User + Method.Update;
    const data = { nick, avatar, sex, birthday, openid };
    return new Promise((resolve) => {
      return this.request(url, data).then((res: RespInfo<UserInfo>) => {
        appRef.user.setData(res.info);
        resolve();
      });
    });
  }

  public token(): Promise<void> {
    const url = config.usersbeasking + Uri.User + Method.Token;
    const data = {};
    return new Promise((resolve) => {
      if (!appContext.global.token) {
        resolve();
        return;
      }
      return this.request(url, data, "GET").then((res: RespInfo<UserInfo>) => {
        appRef.user.setData(res.info);
        // setTimeout(() => {
        //   if (!res.info.openid) hope.wxLogin({ state: WxOptionType.SnsApiBase });
        // }, 100);
        // paymentService.active();
        appNotify.send(NotifyType.UserInfo, NotifyOption.Empty);
        Message.success("登录成功");
        resolve();
      });
    });
  }

  public logoff(): Promise<void> {
    const url = config.usersbeasking + Uri.User + Method.Logoff;
    const data = {};
    return new Promise((resolve) => {
      return this.request(url, data).then(() => {
        localStorage.clear();
        location.reload();
        resolve();
      });
    });
  }
}

export const userService: UserService = new UserService();
