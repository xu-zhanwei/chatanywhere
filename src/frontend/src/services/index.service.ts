import { BaseServices } from "./base.services";

export class IndexService extends BaseServices {
  private static _ins: IndexService = new IndexService();
  public static get ins(): IndexService {
    return this._ins ? this._ins : (this._ins = new IndexService());
  }

  /** 获取名言名句 */
  public getWellKnownSaying(): Promise<{ quote: string; author: string }> {
    const url = "https://bak.zaiwenai.top/get_quote";
    return new Promise<any>((resolve) => {
      this.get(url).then((res: any) => {
        resolve(res);
      });
    });
  }
}

export const indexService: IndexService = IndexService.ins;
