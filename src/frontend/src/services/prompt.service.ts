import { appRef } from "@/models/app.ref";
import { BaseServices } from "./base.services";
import { RoleEnum } from "@/models/@types";
import { getUuid } from "@/utils/common.methods";
import { resolve } from "path";
import { PromptEnum, PromptInfo } from "@/models/prompt";
import { AuthorInfo } from "@/models/user";
import { Message } from "@arco-design/web-vue";

export type GeneratePromptReq = {
  role: RoleEnum;
  content: string;
};

export class PromptService extends BaseServices {
  private static _ins: PromptService = new PromptService();
  public static get ins(): PromptService {
    return this._ins ? this._ins : (this._ins = new PromptService());
  }

  private controllerKey = getUuid();

  /**
   * 获取提示词列表
   */
  public promptList(): Promise<void> {
    const url = "https://jump.zaiwen.top/admin/get_prompt";
    const data = {};
    return new Promise((resolve) => {
      if (appRef.prompt.officialList?.length) {
        resolve();
        return;
      }
      return this.request(url, data, "GET").then((res: PromptInfo[]) => {
        appRef.prompt.setOfficialList(res);
        resolve();
      });
    });
  }
  /** 新增 */
  public async add(list: PromptInfo[]): Promise<void> {
    const url = "https://jump.zaiwen.top/admin/add_prompt";
    const data: PromptInfo[] = list?.map((val) => {
      val.id = getUuid();
      return val;
    });
    return new Promise((resolve) => {
      return this.request(url, data).then(() => {
        appRef.prompt.addOfficialList(data);
        Message.success("创建成功");
        resolve();
      });
    });
  }
  /** 修改 */
  public async update(info: PromptInfo): Promise<void> {
    const url = "https://jump.zaiwen.top/admin/update_prompt/" + info.id;
    const data = info;
    return new Promise((resolve) => {
      return this.request(url, data).then(() => {
        appRef.prompt.updateOfficialList(data);
        Message.success("修改成功");
        resolve();
      });
    });
  }
  /** 删除  */
  public delete(id: string): Promise<void> {
    const url = "https://jump.zaiwen.top/admin/delete_prompt/" + id;
    const data = {};
    return new Promise((resolve) => {
      return this.request(url, data, "DELETE").then(() => {
        appRef.prompt.deleteOfficialList(id);
        Message.success("删除成功");
        resolve();
      });
    });
  }

  /**
   * 生成提示词
   * 基于消息生成的三个更多提示的接口（使用gpt3.5）
   */
  public generate_prompt(message: GeneratePromptReq[], end: (data: string) => void): Promise<string> {
    const url = "https://www.13042332817.top/generate_prompt";
    const data = { message };
    return new Promise<string>((resolve) => {
      this.stopStream(this.controllerKey);
      if (!message?.length) {
        resolve("");
      } else {
        this.stream(this.controllerKey, url, data, () => null, end);
      }
    });
  }

  /**
   * 发送消息, 以流数据形式返回
   */
  public message(key: string, message: GeneratePromptReq[], chunk: (data: string) => void, end: (data: string) => void): void {
    const url = "https://aliyun.zaiwen.top/vision_gemini";
    const data = { message, user_key: appRef.user.data.openai_key };
    return this.stream(key, url, data, chunk, end);
  }
}

export const promptService: PromptService = PromptService.ins;
