/*
 * @Author: zhanwei xu
 * @Date: 2023-08-19 17:30:45
 * @LastEditors: zhanwei xu
 * @LastEditTime: 2024-02-27 10:16:58
 * @Description:
 *
 * Copyright (c) 2024 by zhanwei xu, Tsinghua University, All Rights Reserved.
 */
import { appRef } from "@/models/app.ref";
import { BaseServices } from "./base.services";
import { RespInfo } from "./config";
import { SessionInfo, RoleEnum } from "@/models/@types";
import { AssetInfo } from "@/models/asset";

export class FileService extends BaseServices {
  private static _ins: FileService = new FileService();
  public static get ins(): FileService {
    return this._ins ? this._ins : (this._ins = new FileService());
  }

  /** 上传文件 */
  public uploadFile(file: File): Promise<string> {
    const url = "https://cwjiaoyu.cn/fileApi/share/upload_file/";
    const data = new FormData();
    data.append("file", file);
    return new Promise<string>((resolve) => {
      this.request(url, data).then(async (res: RespInfo<string>) => {
        resolve(res.info);
      });
    });
  }

  /** 发送消息 */
  public async send(key: string, assets: AssetInfo[], question: string, model: string, chunk: (data: string) => void, end: () => void, quote?: string): Promise<void> {
    if (quote) {
      const currSession = appRef.chatFile?.currMenu?.sessions?.find((val) => val.quote === quote);
      appRef.chatFile.removeMenuAnswer(currSession.uid, false);
      question = appRef.chatFile?.currMenu?.sessions?.find((val) => val.uid === quote)?.richtext;
    } else {
      quote = await appRef.chatFile.addMenuAnswer(RoleEnum.User, question);
    }

    const key_ = appRef.user.data.openai_key;
    const messages = appRef.chatFile?.currMenu?.sessions?.map((val) => ({ content: val.richtext, role: val.role }))
    const url = "https://aliyun.zaiwen.top/fileask/fileApi/share/message";
    const data = { assets: assets?.map(val => val.id), messages, model, key: key_ };
    this.stream(
      key,
      url,
      data,
      chunk,
      (data: string) => {
        if (data) appRef.chatFile.addMenuAnswer(RoleEnum.AI, data, quote);
        end();
      },
      (err) => {
        appRef.chatFile.addMenuAnswer(RoleEnum.Tip, err.toString());
      }
    );
  }

  /** 重新发送消息 */
  // public refreshSend(filename: string, quote: string): Promise<string> {
  //   const currSession = appRef.chatFile?.currMenu?.sessions?.find((val) => val.quote === quote);
  //   const session = appRef.chatFile?.currMenu?.sessions?.find((val) => val.uid === quote);
  //   const url = "https://bak.cwjiaoyu.cn/fileApi/share/message/";
  //   const data = { filename, question: session.richtext };
  //   return new Promise<string>((resolve) => {
  //     this.request(url, data)
  //       .then(async (res: { answer: string }) => {
  //         appRef.chatFile.removeMenuAnswer(currSession.uid, false);
  //         appRef.chatFile.addMenuAnswer(RoleEnum.AI, res.answer, quote);
  //         resolve(res.answer);
  //       })
  //       .catch((err) => {
  //         appRef.chatFile.removeMenuAnswer(session.uid, false);
  //         appRef.chatFile.addMenuAnswer(RoleEnum.AI, `Error sending message. Please try again. <br> ${err.stack}`, quote);
  //         resolve(err.stack);
  //       });
  //   });
  // }
}

export const fileService: FileService = FileService.ins;
