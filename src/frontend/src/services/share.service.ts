import { BaseServices } from "./base.services";

export class ShareService extends BaseServices {
  private static _ins: ShareService = new ShareService();
  public static get ins(): ShareService {
    return this._ins ? this._ins : (this._ins = new ShareService());
  }
  /** 发送消息 */
  public send(data: any): Promise<any> {
    const url = "https://bak.zaiwenai.top/dataApi/share/insertData?userId=0";
    const sharedata = {
      "label": "",
      "conversation": data
    }
    return new Promise<any>((resolve, reject) => {
      this.request(url, sharedata)
        .then(async (res) => {
          console.log(res)
          resolve(res);
        })
        .catch((err) => {
          console.log(err)
          reject(err.stack);
        });
    });
  }
  public getrecords(uuid: String): Promise<string> {
    const url = "https://bak.zaiwenai.top/dataApi/share/getDataViaUuid?uuid=" + uuid;
    return new Promise<string>((resolve, reject) => {
      this.get(url)
        .then(async (res) => {
          console.log(res)
          resolve(JSON.stringify(res));
        })
        .catch((err) => {
          console.log(err)
          reject(err.stack);
        });
    });
  }
}
export const shareService: ShareService = ShareService.ins;