export const config = {
  httpHost: "https://bak.zaiwen.org.cn/",
  cwjiaoyu: " https://www.cwjiaoyu.cn/",
  beasking: " https://scishort.beasking.top/",
  usersbeasking: "https://user.chatcns.com/",

  httpHostTest: "http://192.168.1.41:9180", // 本地

  version: "1.0",
};
export enum Uri {
  FileApi_Share = "fileApi/share/",
  Media = "media/",
  Gallery = "gallery/",
  Image = "image/",
  Article = "article/",
  User = "user/",
  Common_Sms = "common/sms/",
  Asset = "asset/",
  Synchronous = "synchronous/",
  Dev = "dev/",
}

export enum Method {
  Add = "add",
  Delete = "delete",
  Updated = "updated",
  Update = "update",
  List = "list",
  Get = "get",
  Token = "token",
  Logoff = "logoff",
  Register = "register",
  ForgotPassword = "forgotpassword",
  Login = "login",
  LoginOut = "loginOut",
  Password = "password",
  Code = "code",
  Upload_File = "upload_file",
  Send = "send",
  Verify = "verify",
  Up = "up",
  Down = "down",
  Session = "session",
  Message = "message",
  Clear = "clear",
}

export enum RespResultCode {
  Success = 0,

  TokenEmpty = 2, // Token 为空
  TokenExpire = 3, // Token 过期
  TokenMiss = 4, // Token 错误
  SendCodeErr = 11,
  CodeErr = 13,
  CodeExpire = 14,
  DataCorrupted = 15, // 解密加密code失败
  ExtractPhoneErr = 16, // 解密后， 从中提取手机号码失败
  PhoneExist = 21,
  PhoneNotFound = 22, // 未找到手机号，用户不存在
  UnkownLoginType = 31, // 未知登录类型
  AssetAddFail = 41, // 资产添加失败
  AssetNotFound = 42, // 资产不存在
  SyncStoreFail = 51, // 同步记录存储失败
  SyncNotFound = 52, // 同步记录不存在
}

export enum NetStatusCode {
  Success = 200,
  Error = 500,
}

export interface StatusResp {
  code: RespResultCode;
  error: string;
  token: string;
}

export interface StatusInfo {
  code: number;
  error: string;
}

export type RespInfo<T = void> = {
  info: T;
  list: T[];
  total?: number;
  page?: number;
  baseurl?: string;
  status: StatusInfo;
  token?: string;
  code?: string;
};
