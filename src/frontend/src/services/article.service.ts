import { BaseServices } from "./base.services";
import { RoleEnum } from "@/models/@types";
import { Method, RespInfo, Uri, config } from "./config";
import { appRef } from "@/models/app.ref";
import { ArticleInfo } from "@/models/article";

export type GeneratePromptReq = {
  role: RoleEnum;
  content: string;
};

export class ArticleService extends BaseServices {
  private static _ins: ArticleService = new ArticleService();
  public static get ins(): ArticleService {
    return this._ins ? this._ins : (this._ins = new ArticleService());
  }

  public list(page: number = 1, number: number = 100): Promise<ArticleInfo[]> {
    const url = config.beasking + Uri.Article + Method.List;
    const data = { page, number };
    return new Promise<ArticleInfo[]>((resolve) => {
      const list = appRef.article.list(page, number);
      if (list?.length) {
        resolve(list);
      } else {
        this.request(url, data, "GET").then((res: RespInfo<ArticleInfo>) => {
          appRef.article.setList(page, number, res.list);
          appRef.article.total = res.total;
          resolve(res.list);
        });
      }
    });
  }
  public getOne(id: string): Promise<ArticleInfo> {
    const url = config.beasking + Uri.Article + Method.Get;
    const data = { id };
    return new Promise<ArticleInfo>((resolve) => {
      const item = appRef.article.get(id);
      if (item?.id) {
        resolve(item);
      } else {
        this.request(url, data, "GET").then((res: RespInfo<ArticleInfo>) => {
          resolve(res.info);
        });
      }
    });
  }
}

export const articleService: ArticleService = ArticleService.ins;
