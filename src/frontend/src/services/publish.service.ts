import { BaseServices } from "./base.services";
import { RoleEnum } from "@/models/@types";

export type GeneratePromptReq = {
  role: RoleEnum;
  content: string;
};

export class PublishService extends BaseServices {
  private static _ins: PublishService = new PublishService();
  public static get ins(): PublishService {
    return this._ins ? this._ins : (this._ins = new PublishService());
  }

  public search(message: string): Promise<string> {
    const url = "https://www.gaosijiaoyu.cn/search_published";
    const data = { message};
    return new Promise<string>((resolve) => {
      this.request(url, data).then((res: string) => {
        resolve(res);
      });
    });
  }
}

export const publishService: PublishService = PublishService.ins;
