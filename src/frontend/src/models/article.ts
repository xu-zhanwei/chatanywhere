export type ArticleInfo = {
  id: string;
  title: string; // 标题
  desc: string; // 描述
  created: number; // 创建时间
  author: string[]; // 作者
  content: string; // 内容
  quote: string; // 置顶外链  对应下方的 quotes 中某条的 id
  tags?: string[];
  cover?: string; // 封面
  quotes?: ArticleQuoteInfo[]; // 相关（外站）链接
  originals?: ArticleQuoteInfo[]; // 原文链接
};

export type ArticleQuoteInfo = {
  id: string;
  name: string; // 站名
  href: string; // 作品链接
  index?: string; // 主页链接
};

export class ArticleModel {
  private _list: ArticleInfo[] = [];
  public total: number = 0;

  public list(page: number, number: number): ArticleInfo[] {
    return this._list.slice(page - 1 * number, page * number);
  }
  public get(id: string): ArticleInfo {
    return this._list.find((val) => val.id === id);
  }

  public isEmpty(page: number, number: number): boolean {
    return this._list?.length < page * number;
  }

  public setList(page: number, number: number, list: ArticleInfo[]): void {
    if (this._list?.length > page * number) {
      this._list = list;
    } else {
      this._list = (this._list || []).concat(list);
    }
  }
}
