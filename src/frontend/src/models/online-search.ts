import { getUuid } from "@/utils/common.methods";
import { SiderMenuEnum } from "./@types";
import { chatDB } from "@/db/chat";
import { SyncActionEnum, synchronousService } from "@/services/synchronous.service";

export type OnlineSearchInfo = {
  uid: string;
  label: string;
  type: SiderMenuEnum;
  database: DatabaseEnum;
  childrens?: OnlineSearchInfo[];
  question?: string;
  answer?: string;
};

export enum DatabaseEnum {
  Default = "default",
  Bing = "bing",
  Arxiv = "arxiv",
  Publish = "publish",
}

export const OnlineSearchModelEnumOption = [
  {
    key: DatabaseEnum.Default,
    name: "默认",
  },
  {
    key: DatabaseEnum.Bing,
    name: "Bing",
  },
  {
    key: DatabaseEnum.Arxiv,
    name: "Arxiv",
  },
  {
    key: DatabaseEnum.Publish,
    name: "学术",
  },
];

export type LOCAL_STORAGEInfo = {
  currKey?: string;
  menus?: OnlineSearchInfo[];
};

export class OnlineSearchModel {
  public LOCAL_STORAGE_KEY = "online-search";

  private _data = <LOCAL_STORAGEInfo>{};

  private _list: OnlineSearchInfo[] = [];
  private _max: number = 10; // 最大可保存多少条历史记录

  constructor() {}

  public async setData(): Promise<void> {
    let dataStr = (await chatDB.getStr(this.LOCAL_STORAGE_KEY))?.value ;
    this._data = dataStr ? JSON.parse(dataStr) : {};
  }
  /** 左侧目录 */
  public async menus(): Promise<OnlineSearchInfo[]> {
    if (!this._data?.currKey) {
      await this.setData();
    }
    return this._data?.menus || [];
  }

  public async add(type: SiderMenuEnum, parent?: string): Promise<void> {
    const label = (type === SiderMenuEnum.Group ? "合集" : "会话") + getUuid(5);
    const uid = getUuid();
    const item: OnlineSearchInfo = { uid, label, type, database: DatabaseEnum.Default };
    if (parent) {
      this._data?.menus?.map((val) => {
        if (val.uid === parent) {
          val.childrens = (val.childrens || []).concat(item);
        }
        return val;
      });
    } else {
      this._data = {
        currKey: uid,
        menus: (this._data?.menus || []).concat(item),
      };
    }
    synchronousService.session(this.LOCAL_STORAGE_KEY, SyncActionEnum.Add, item, parent);
    await this.setLocalStorage("add");
  }

  public async updateEmuse(menus: OnlineSearchInfo[]) {
    this._data.menus = menus;
    await this.setLocalStorage("update");
  }

  public async edit(uid: string, label: string): Promise<void> {
    let item = {} as OnlineSearchInfo;
    let parent = undefined;
    this._data.menus?.forEach((menu) => {
      if (menu.uid === uid) {
        menu.label = label;
        item = menu;
      }
      menu.childrens?.forEach((child) => {
        if (child.uid === uid) {
          child.label = label;
          item = child;
          parent = menu.uid;
        }
      });
    });
    synchronousService.session(this.LOCAL_STORAGE_KEY, SyncActionEnum.Update, item, parent);

    await this.setLocalStorage("edit");
  }

  public async remove(uid: string): Promise<void> {
    this._data.menus = this._data.menus?.filter((val) => {
      val.childrens = val.childrens?.filter((child) => child.uid !== uid);
      return val.uid !== uid;
    });
    this._data.currKey = this._data.menus[0]?.uid || "";
    synchronousService.session(this.LOCAL_STORAGE_KEY, SyncActionEnum.Delete, { uid } as OnlineSearchInfo);
    await this.setLocalStorage("remove");
  }

  public async setCurrKey(uid: string): Promise<void> {
    this._data.currKey = uid;
    await this.setLocalStorage("setCurrKey");
  }

  public get currKey(): string {
    return this._data?.currKey;
  }

  public get currMenu(): OnlineSearchInfo {
    if (!this._data?.menus?.length) return;
    const key = this.currKey;
    if (this._data?.menus) {
      for (const item of this._data?.menus) {
        if (item.uid === key) {
          return item;
        } else {
          if (item?.childrens) {
            for (const child of item?.childrens) {
              if (child.uid === key) return child;
            }
          }
        }
      }
    }
    return null;
  }

  // public get list(): OnlineSearchInfo[] {
  //   if (!this._list?.length) {
  //     const dataStr = localStorage.getItem(this.LOCAL_STORAGE_KEY);
  //     this._list = dataStr ? JSON.parse(dataStr) : {};
  //   }
  //   return this._list;
  // }

  /** 修改记录中的 问题及答案 */
  public async update(uid: string, question: string, answer: string): Promise<void> {
    let item = {} as OnlineSearchInfo;
    let parent = undefined;
    if (this._data.menus) {
      for (const menu of this._data.menus) {
        if (menu.uid === uid) {
          menu.label = question.slice(0, 10);
          menu.question = question;
          menu.answer = answer;
          item = menu;
        }
        if (menu.childrens) {
          for (const child of menu.childrens) {
            if (child.uid === uid) {
              child.label = question.slice(0, 10);
              child.question = question;
              child.answer = answer;
              item = child;
              parent = menu.uid;
            }
          }
        }
      }
    }
    synchronousService.session(this.LOCAL_STORAGE_KEY, SyncActionEnum.Update, item, parent);
    await this.setLocalStorage("add");
  }

  // public item(uid?: string): OnlineSearchInfo {
  //   if (uid) {
  //     return this.list?.find((val) => val.uid === uid);
  //   }
  //   return this.list[0];
  // }

  /** 清空 */
  public async clear(): Promise<void> {
    this._data = {};
    synchronousService.clearSession(this.LOCAL_STORAGE_KEY);
    await this.setLocalStorage("clear");
  }

  private async setLocalStorage(source: string): Promise<void> {
    console.log("setLocalStoragesetloca >>> source :", source, this._data);
    await chatDB.set(this.LOCAL_STORAGE_KEY, JSON.stringify(this._data));
    // localStorage.setItem(this.LOCAL_STORAGE_KEY, JSON.stringify(this._data));
  }
}
