import { getUuid } from "@/utils/common.methods";
import { SessionInfo, RoleEnum, SiderMenuEnum } from "./@types";
import { Message } from "@arco-design/web-vue";
import { appRef } from "./app.ref";
import { chatDB } from "@/db/chat";
import { AssetInfo } from "./asset";
import { SyncActionEnum, synchronousService } from "@/services/synchronous.service";

export type ChatWorkMenuInfo = {
  uid: string;
  label: string;
  type: SiderMenuEnum;
  sessions?: SessionInfo[];
  childrens?: ChatWorkMenuInfo[];
  modeluid?: string;
  prompt?: string;
  continuous?: boolean;
  article?: string;
};

export type ChatWorkInfo = {
  currKey?: string;
  menus?: ChatWorkMenuInfo[];
};

export type ChatWorkModelInfo = {
  value: string;
  label: string;
  url: string;
  description: string;
  vision: number;
};

export class ChatWorkModel {
  public LOCAL_STORAGE_KEY = "chat-work";

  private _data = <ChatWorkInfo>{};
  private _models: ChatWorkModelInfo[] = [];

  constructor() {}

  public async setData(): Promise<void> {
    let dataStr = (await chatDB.getStr(this.LOCAL_STORAGE_KEY))?.value;
    this._data = dataStr ? JSON.parse(dataStr) : {};
  }

  public get models(): ChatWorkModelInfo[] {
    return this._models;
  }
  public getModel(value: string): ChatWorkModelInfo {
    return value ? this._models?.find((val) => val.value === value) || this._models[0] : this._models[0];
  }
  public setModels(list: ChatWorkModelInfo[]): void {
    this._models = list;
  }

  /** 左侧目录 */
  public async menus(): Promise<ChatWorkMenuInfo[]> {
    if (!this._data?.currKey) {
      await this.setData();
    }
    return this._data?.menus || [];
  }

  public async add(type: SiderMenuEnum, parent?: string): Promise<void> {
    await this.menus();
    const label = (type === SiderMenuEnum.Group ? "合集" : "会话") + getUuid(5);
    const uid = getUuid();
    const modeluid = this._models[0]?.value || "";
    const item: ChatWorkMenuInfo = { uid, label, type, modeluid };
    if (parent) {
      this._data?.menus?.map((val) => {
        if (val.uid === parent) {
          val.childrens = (val.childrens || []).concat(item);
        }
        return val;
      });
    } else {
      this._data.menus = (this._data?.menus || []).concat(item);
    }
    this._data.currKey = uid;
    synchronousService.session(this.LOCAL_STORAGE_KEY, SyncActionEnum.Add, item, parent);
    await this.setLocalStorage("add");
  }

  public async edit(uid: string, label: string): Promise<void> {
    let item = {} as ChatWorkMenuInfo;
    let parent = "";
    this._data.menus?.forEach((menu) => {
      if (menu.uid === uid) {
        menu.label = label;
        item = menu;
      }
      menu.childrens?.forEach((child) => {
        if (child.uid === uid) {
          child.label = label;
          item = child;
          parent = menu.uid;
        }
      });
    });
    synchronousService.session(this.LOCAL_STORAGE_KEY, SyncActionEnum.Update, item, parent);
    await this.setLocalStorage("edit");
  }

  public async setCurrArticle(article: string): Promise<void> {
    const uid = this.currKey;
    this._data.menus?.forEach((menu) => {
      if (menu.uid === uid) menu.article = article;
      menu.childrens?.forEach((child) => {
        if (child.uid === uid) child.article = article;
      });
    });
    await this.setLocalStorage("setCurrArticle");
  }

  public async remove(uid: string): Promise<void> {
    this._data.menus = this._data.menus?.filter((val) => {
      val.childrens = val.childrens?.filter((child) => child.uid !== uid);
      return val.uid !== uid;
    });
    this._data.currKey = this._data.menus[0]?.uid || "";
    synchronousService.session(this.LOCAL_STORAGE_KEY, SyncActionEnum.Delete, { uid } as ChatWorkMenuInfo);
    await this.setLocalStorage("remove");
  }

  public async update(menus: ChatWorkMenuInfo[]) {
    this._data.menus = menus;
    await this.setLocalStorage("update");
  }

  public async setCurrKey(uid: string): Promise<void> {
    this._data.currKey = uid;
    await this.setLocalStorage("setCurrKey");
  }

  public get currKey(): string {
    return this._data?.currKey;
  }

  public get currMenu(): ChatWorkMenuInfo {
    if (!this._data?.menus?.length) return;
    const key = this.currKey;
    for (const item of this._data?.menus) {
      if (item.uid === key) {
        if (!item?.modeluid) item.modeluid = this.models[0]?.value || "";
        return item;
      } else {
        if (item?.childrens) {
          for (const child of item?.childrens) {
            if (child.uid === key) {
              if (!child?.modeluid) child.modeluid = this.models[0]?.value || "";
              return child;
            }
          }
        }
      }
    }
    return null;
  }

  /** 写入提示词， 使ai扮演角色 */
  public async updatedPrompt(uid: string, promptid: string) {
    let continuous = false;
    if (promptid) {
      const prompt = await appRef.prompt.get(promptid);
      if (prompt?.continuous) {
        continuous = true;
        Message.success("该 prompt 支持连续对话，已为你自动开启");
      } else {
        Message.success("该 prompt 不持连续对话，已为你自动关闭");
      }
    } else {
      Message.success("清除 prompt");
    }

    let item = {} as ChatWorkMenuInfo;
    let parent = "";
    for (const menu of this._data.menus) {
      if (menu.uid === uid) {
        menu.prompt = promptid;
        menu.continuous = continuous;
        item = {} as ChatWorkMenuInfo;
        item = menu;
      }
      if (item?.childrens) {
        for (const child of item?.childrens) {
          if (child.uid === uid) {
            child.prompt = promptid;
            child.continuous = continuous;
            item = child;
            parent = menu.uid;
          }
        }
      }
    }
    synchronousService.session(this.LOCAL_STORAGE_KEY, SyncActionEnum.Update, item, parent);
    await this.setLocalStorage("updatedPrompt");
  }

  /**
   * 创建会话
   * @param role
   * @param richtext
   * @param quote
   * @returns 会话 uid
   */
  public async addMenuAnswer(role: RoleEnum, richtext: string, quote?: string, assets: AssetInfo[] = []): Promise<string> {
    const uid = getUuid();
    const key = this._data.currKey;
    const sessions: SessionInfo = { uid, role, timestamp: Date.now(), richtext, quote, assets };
    for (const item of this._data.menus) {
      if (item.uid === key) {
        if (!item.sessions?.length) {
          this.edit(item.uid, richtext?.slice(0, 10));
        }
        item.sessions = (item.sessions || []).concat(sessions);
      }
      if (item?.childrens) {
        for (const child of item?.childrens) {
          if (child.uid === key) {
            if (!child.sessions?.length) {
              this.edit(child.uid, richtext?.slice(0, 10));
            }
            child.sessions = (child.sessions || []).concat(sessions);
          }
        }
      }
    }
    synchronousService.message(SyncActionEnum.Add, key, sessions);
    await this.setLocalStorage("addMenuAnswer");
    return uid;
  }

  public async removeMenuAnswer(uid: string, isnotice: boolean = true) {
    console.log(this._data.menus, uid);
    for (const item of this._data.menus) {
      if (item.uid === this._data.currKey) {
        item.sessions = item.sessions?.filter((val) => val.uid !== uid);
      }
      if (item?.childrens) {
        for (const child of item?.childrens) {
          if (child.uid === this._data.currKey) {
            child.sessions = child.sessions?.filter((val) => val.uid !== uid);
          }
        }
      }
    }
    if (isnotice) Message.success("删除成功");
    synchronousService.message(SyncActionEnum.Delete, this._data.currKey, { uid } as SessionInfo);
    await this.setLocalStorage("removeMenuAnswer");
  }

  /** 清空 */
  public async clear(): Promise<void> {
    this._data = {};
    synchronousService.clearSession(this.LOCAL_STORAGE_KEY);
    await this.setLocalStorage("clear");
  }

  private async setLocalStorage(source: string): Promise<void> {
    console.log("setLocalStoragesetloca >>> source :", source, this._data);
    await chatDB.set(this.LOCAL_STORAGE_KEY, JSON.stringify(this._data));
    // localStorage.setItem(this.LOCAL_STORAGE_KEY, JSON.stringify(this._data));
  }
}
