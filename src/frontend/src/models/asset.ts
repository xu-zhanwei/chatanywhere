import { deduplication } from "@/utils/common.methods";

export interface AssertsToken {
  bucket: string;
  domain: string;
  expire: string;
  limit: string;
  status: { code: number };
  token: string;
}
/** 资源类型 */
export enum AssetType {
  Single = 0,
  Group = 1,
  Window = 2,
  Android = 3,
  AudioFrequency = 4,
  Video = 5,
  Portrait = 6,
  Icon = 7, // 系统/产品 图标
}

export enum LanguageType {
  "Zh" = "zh",
  "En" = "en",
}

export type AssetToken = {
  token: string;
  region: RegionEnum;
  bucket: string;
  domain: string;
};

export enum RegionEnum {
  z0 = "z0",
  z1 = "z1",
  z2 = "z2",
  na0 = "na0",
  as0 = "as0",
  cnEast2 = "cn-east-2",
}

export interface AssetInfo {
  id: string;
  name: string;
  remark: string;
  md5: string;
  format: string;
  size: number;
  url: string;
  small: string;
  snapshot: string;
  language: string;
  owner: string;
  created: number;
  version: string;
  weight: number;
  height: number;
  meta: string;
  type?: AssetType;
  status?: AssetStatus;
}

export enum AssetStatus {
  Default = 0,
  Wicked = 1,
  Public = 2,
}


export type AlbumInfo = {
  uid: string;
  group: string;
  name: string;
  cover?: string;
  remark?: string;
  assets?: AssetInfo[];
};

export enum SwiperType {
  Subject = 1,
  Expert = 2,
  Album = 3,
}

export type SwiperInfo = {
  asset: AssetInfo;
  name?: string;
  type?: SwiperType;
  quote?: string;
};
export type RecommendInfo = {
  owner: AssetInfo;
  status?: string;
  type?: SwiperType;
  targets?: string[];
};

export class AssetModel {
  private _swiper: SwiperInfo[] = [];
  private _album: AlbumInfo[] = [];
  private _recommend: RecommendInfo[] = [];
}
