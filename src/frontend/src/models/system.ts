import { weChatService } from "@/services/wechat.service";

export type SystemInfo = {
  viewWidth: number;
  viewHeight: number;
};

export type EnvironmentType = "miniprogram" | "browser";

export class SystemModel {
  private _data = <SystemInfo>{};
  private _environment: EnvironmentType;

  constructor() { }

  public init(): void {
    console.log(navigator.userAgent);
    // @ts-ignore
    if (window?.__wxjs_environment === "miniprogram") {
      this._environment = "miniprogram";
      weChatService.signature();
    } else {
      this._environment = "browser";
    }
  }

  public get data(): SystemInfo {
    const { clientWidth, clientHeight } = document.documentElement;
    this._data = {
      viewWidth: clientWidth,
      viewHeight: clientHeight,
    };
    return this._data;
  }
}
