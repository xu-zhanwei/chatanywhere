/*
 * @Author: zhanwei xu
 * @Date: 2023-08-19 17:30:45
 * @LastEditors: zhanwei xu
 * @LastEditTime: 2024-02-02 09:26:22
 * @Description: 
 * 
 * Copyright (c) 2024 by zhanwei xu, Tsinghua University, All Rights Reserved. 
 */
import { AssetInfo } from "./asset";

export type SiderMenuInfo = {
  uid: string;
  label: string;
  type: SiderMenuEnum;
  childrens: SiderMenuInfo[];
};

export enum SiderMenuEnum {
  Group = "group",
  Leaf = "leaf",
}

export enum RoleEnum {
  System = "system",
  User = "user",
  AI = "assistant",
  Tip = "tip",
}

export type SessionInfo = {
  uid: string;
  role: RoleEnum;
  timestamp: number;
  richtext: string;
  /** 携带的文件 */
  assets?: AssetInfo[];
  /** 音频 */
  audioFile?: File;
  duration?: number;
  /** 判断该会话是否正在载入 */
  loading?: boolean;
  /** 回答对问题的引用 （该回答针对于那条问题） */
  quote?: string;
};

export type AssessionInfo = {
  uid: string;
  role: RoleEnum;
  timestamp: number;
  richtext: string;
  /** 判断该会话是否正在载入 */
  loading?: boolean;
  /** 回答对问题的引用 （该回答针对于那条问题） */
  quote?: string;
};

export type KeyValue<T = string, U = string> = {
  key: T;
  value: U;
};

/** 登录类型 */
export enum LoginType {
  Password = 1,
  Phone = 2,
}

export enum SexEnum {
  WoMan = 0,
  Man = 1,
}

/**
 * FAQ的问题列表
 */
export type FaqItem = {
  id: number;
  title?: string;
  content: string;
};
