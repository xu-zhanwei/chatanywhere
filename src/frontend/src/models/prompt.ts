import { chatDB } from "@/db/chat";
import { getUuid } from "@/utils/common.methods";

export type PromptInfo = {
  id: string;
  role: string;
  value: string;
  workflow?: string; // 提示词的工作流, 告知用户，以便用户更好地使用
  type?: PromptEnum;
  author?: string;
  tags?: string[];
  continuous?: number; // 0 不连续对话， 1 连续对话
};

export enum PromptEnum {
  Default,
  Custom,
}

export class PromptModel {
  public LOCAL_STORAGE_KEY = "chat-prompts";
  private _officialList: PromptInfo[] = []; // 官方提示词列表
  private _list: PromptInfo[] = []; // 用户 自主创建提示词列表

  public get officialList(): PromptInfo[] {
    return this._officialList;
  }
  public setOfficialList(list: PromptInfo[]): void {
    this._officialList = list
      ?.map((val) => {
        val.continuous ? null : (val.continuous = 0);
        return val;
      })
      ?.reverse();
  }
  public addOfficialList(list: PromptInfo[]): void {
    this._officialList = [...new Set([...list, ...this._officialList])];
  }
  public updateOfficialList(info: PromptInfo): void {
    this._officialList = this._officialList?.map((val) => {
      if (val.id === info.id) val = info;
      return val;
    });
  }
  public deleteOfficialList(id: string): void {
    this._officialList = this._officialList?.filter((val) => val.id !== id);
  }

  public async tags(): Promise<string[]> {
    const list = (await this.list())?.reduce((pre: string[], val) => (pre = pre.concat(...(val?.tags || []))), []);
    return [...new Set(list)];
  }

  public async setData(): Promise<void> {
    let dataStr = (await chatDB.getStr(this.LOCAL_STORAGE_KEY))?.value ;
    this._list = dataStr ? JSON.parse(dataStr) : [];
  }

  public async list(keyword: string = "", tags: string[] = []): Promise<PromptInfo[]> {
    if (!this._list?.length) {
      await this.setData();
    }
    return [...(this._list || []), ...(this.officialList || [])]?.filter((val) => val.role?.includes(keyword) || val.workflow?.includes(keyword) || val.value?.includes(keyword))?.filter((val) => (tags?.length ? val.tags?.some((tag) => tags?.includes(tag)) : true));
  }

  public async get(id: string): Promise<PromptInfo> {
    return (await this.list())?.find((val) => val.id === id);
  }

  public async add(role: string, value: string, workflow?: string, continuous: number = 0): Promise<void> {
    const id = getUuid();
    const item = { id, role, type: PromptEnum.Custom, value, workflow, continuous };
    this._list = [item, ...this._list];
    await this.setLocalStorage("add");
  }

  public async remove(id: string): Promise<void> {
    this._list = this._list?.filter((val) => val.id !== id);
    await this.setLocalStorage("remove");
  }

  public async update(info: PromptInfo): Promise<void> {
    this._list = this._list?.map((val) => {
      if (val.id === info.id) val = info;
      return val;
    });
    await this.setLocalStorage("update");
  }
  private async setLocalStorage(source: string): Promise<void> {
    console.log("setLocalStoragesetloca >>> source :", source, this._list);
    await chatDB.set(this.LOCAL_STORAGE_KEY, JSON.stringify(this._list || []));
    // localStorage.setItem(this.LOCAL_STORAGE_KEY, JSON.stringify(this._list));
  }
}
