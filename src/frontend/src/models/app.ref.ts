import { Ref, ref } from "vue";
import { ChatFileModel } from "./chat-file";
import { ChatWorkModel } from "./chat-work";
import { UserModel } from "./user";
import { AIDrawModel } from "./aidraw";
import { OnlineSearchModel } from "./online-search";
import { ThemeEnum } from "./app.context";
import { MindMapModel } from "./mind-map";
import { PromptModel } from "./prompt";
import { MediaModel } from "./media";
import { ArticleModel } from "./article";

export class AppRef {
  private static _ins: Ref = ref(new AppRef());

  public theme: ThemeEnum = ThemeEnum.Dark;
  public isOpen: boolean = false;

  private _user: UserModel = new UserModel();
  private _chatFile: ChatFileModel = new ChatFileModel();
  private _aidraw: AIDrawModel = new AIDrawModel();
  private _chatWork: ChatWorkModel = new ChatWorkModel();
  private _onlineSearch: OnlineSearchModel = new OnlineSearchModel();
  private _mindMap: MindMapModel = new MindMapModel();
  private _prompt: PromptModel = new PromptModel();
  private _media: MediaModel = new MediaModel();
  private _article: ArticleModel = new ArticleModel();

  constructor() {}

  public static get ins(): AppRef {
    return this._ins?.value ? this._ins.value : (this._ins.value = new AppRef());
  }

  /** 用户类 */
  public get user(): UserModel {
    return this._user;
  }

  /** 文档分析 */
  public get chatFile(): ChatFileModel {
    return this._chatFile;
  }

  /** AI 绘画 */
  public get aidraw(): AIDrawModel {
    return this._aidraw;
  }

  /** 高效工作 */
  public get chatWork(): ChatWorkModel {
    return this._chatWork;
  }

  /** 联网搜索 */
  public get onlineSearch(): OnlineSearchModel {
    return this._onlineSearch;
  }

  /** 思维导图 */
  public get mindMap(): MindMapModel {
    return this._mindMap;
  }

  /** prompts */
  public get prompt(): PromptModel {
    return this._prompt;
  }

  /** 多媒体 */
  public get media(): MediaModel {
    return this._media;
  }

  /** 文章 */
  public get article(): ArticleModel {
    return this._article;
  }
}
/** 全局响应式 状态管理池 */
export const appRef: AppRef = AppRef.ins;
