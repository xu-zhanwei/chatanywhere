/** 操作类型 */
export enum WxOptionType {
  Default, // LoginVue 默认操作
  SnsApiBase,
}

export type WxConfigInfo = {
  nonce: string;
  sh1: string;
  timestamp: string;
};

export class Wx {
  private static _instance: Wx;
  // 公众号（在线使用）
  public static APPID: string = "wxf8afb51c42506c06";

  // 测试号基本数据;
  // public static APPID: string = "wxea6611dfdc3ecc4f";
  // public static SECRET: string = "768236188d8b83365529111cc5ade47f";

  public code: string = "";
  public access_token: string = "";
  public openid: string = "";

  public config = <WxConfigInfo>{};
  /** 回调的参数 */
  public state: WxOptionType = WxOptionType.Default;

  /** instance */
  public static get instance(): Wx {
    if (!this._instance) this._instance = new Wx();
    return this._instance;
  }

  /**
   * 跳转微信授权登录  默认 ：静默授权
   * @param STATE 自定义回传参数
   * @param scope 表示授权的作用域，多个可以用逗号隔开，snsapi_base表示静默授权，snsapi_userinfo表示非静默授权
   */
  public static wxLogin(STATE = WxOptionType.Default, scope: "snsapi_base" | "snsapi_userinfo" = "snsapi_base") {
    console.log("isWeiXinEvn", this.isWeiXinEvn);
    if (!this.isWeiXinEvn) return;
    let redirect_uri = escape(location.href);
    const response_type = "code";
    const url = `https://open.weixin.qq.com/connect/oauth2/authorize?appid=${Wx.APPID}&redirect_uri=${redirect_uri}&response_type=${response_type}&scope=${scope}&state=${STATE}#wechat_redirect`;
    window.location.href = url;
  }

  public static get isWeiXinEvn(): boolean {
    //@ts-ignore
    return typeof WeixinJSBridge === "object" && typeof WeixinJSBridge.invoke === "function";
  }

  /**
   * 微信内置浏览器调起支付
   * @param appId 公众号ID，由商户传入
   * @param timeStamp  时间戳，自1970年以来的秒数
   * @param nonceStr  随机串
   * @param package  JSAPI下单接口返回的prepay_id参数值，提交格式如：prepay_id=***
   * @param signType 签名类型，默认为RSA，仅支持RSA。
   * @param paySign 签名，使用字段AppID、timeStamp、nonceStr、package计算得出的签名值
   */
  public static getBrandWCPayRequest(param: { appId: string; timeStamp: string; nonceStr: string; package: string; signType: string; paySign: string }): Promise<void> {
    return new Promise((resolve, reject) => {
      //@ts-ignore
      WeixinJSBridge.invoke("getBrandWCPayRequest", param, (res) => {
        if (res.err_msg == "get_brand_wcpay_request:ok") {
          // 使用以上方式判断前端返回,微信团队郑重提示：
          //res.err_msg将在用户支付成功后返回ok，但并不保证它绝对可靠。
          resolve(res);
        } else {
          reject(res);
        }
      });
    });
  }

  public setConfig(data: WxConfigInfo): void {
    this.config = data;
    // @ts-ignore
    wx.config({
      debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
      appId: Wx.APPID, // 必填，公众号的唯一标识
      timestamp: data.timestamp, // 必填，生成签名的时间戳
      nonceStr: data.nonce, // 必填，生成签名的随机串
      signature: data.sh1, // 必填，签名
      jsApiList: ["chooseImage", "getLocalImgData"], // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
    });
  }
}
