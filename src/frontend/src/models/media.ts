import { AssetInfo } from "./asset";
import { UserInfo } from "./user";

export class MediaModel {
  private _gallery: GalleryModel = new GalleryModel();

  public get gallery(): GalleryModel {
    return this._gallery;
  }
}

export type GalleryImageInfo = {
  small?: string;  // 兼容之前画廊格式
  image_id: string; // 兼容之前画廊格式

  asset?: AssetInfo;
  prompt: string;
  negativePrompt?: string;
  author?: UserInfo;
  // C DATA
  blob?: Blob;
};

export class GalleryModel {
  private _list: GalleryImageInfo[] = [];
  public baseurl: string = "";
  public total: number = 0;

  public list(page: number, number: number): GalleryImageInfo[] {
    return this._list.slice(page - 1 * number, page * number);
  }

  public isEmpty(page: number, number: number): boolean {
    return this._list?.length < page * number;
  }

  public setList(page: number, number: number, list: GalleryImageInfo[]): void {
    if (this._list?.length > page * number) {
      this._list = list;
    } else {
      this._list = (this._list || []).concat(list);
    }

    // if (this._list?.length > 500) this._list.slice();
    // this._list = (this._list || []).concat(list);
  }
}
