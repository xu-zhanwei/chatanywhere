/*
 * @Author: zhanwei xu
 * @Date: 2023-08-19 17:30:45
 * @LastEditors: zhanwei xu
 * @LastEditTime: 2024-02-02 16:14:18
 * @Description:
 *
 * Copyright (c) 2024 by zhanwei xu, Tsinghua University, All Rights Reserved.
 */
import { Site } from "@/config/base";
import { appContext } from "@/models/app.context";
import { appNotify, NotifyOption, NotifyType } from "@/models/app.notify";
import { appRef } from "@/models/app.ref";
import { setDocumentTitle } from "@/utils/common.methods";
import { createRouter, createWebHashHistory, createWebHistory, RouteRecordRaw } from "vue-router";

// 2. 定义一些路由
// 每个路由都需要映射到一个组件。
const routes: Readonly<RouteRecordRaw[]> = [
  { name: "index", path: "/", component: () => import("@/views/index/Index.vue"), meta: { transitionName: "slide-left", title: Site.name } },
  {
    name: "chat",
    path: "/chat",
    component: () => import("@/views/Chat.vue"),
    meta: { transitionName: "slide-left", title: "chat" },
    children: [
      // { path: "working-edition", component: () => import("@/views/WorkingEdition/index.vue"), meta: { transitionName: "slide-left", title: "高效问答" } },
      { path: "working-edition", component: () => import("@/views/WorkingEdition.vue"), meta: { transitionName: "slide-left", title: "高效问答" } },
      // { path: "youth-version", component: () => import("@/views/YouthVersion.vue"), meta: { transitionName: "slide-left", title: "青春版" } },
      { path: "online-search", component: () => import("@/views/OnlineSearch.vue"), meta: { transitionName: "slide-left", title: "在线检索" } },
      { path: "share/:uuid", component: () => import("@/views/Share.vue"), meta: { transitionName: "slide-left", title: "分享" } },
      { path: "aidraw", component: () => import("@/views/Aidraw.vue"), meta: { transitionName: "slide-left", title: "随心作画" } },
      { path: "documentQA", component: () => import("@/views/DocumentQA.vue"), meta: { transitionName: "slide-left", title: "文档分析" } },
      { path: "mind-map", component: () => import("@/views/MindMap.vue"), meta: { transitionName: "slide-left", title: "思维导图" } },
      { path: "faq", component: () => import("@/views/FAQ.vue"), meta: { transitionName: "slide-left", title: "FAQ" } },
    ],
  },
  { name: "voice", path: "/voice", component: () => import("@/views/voice/index.vue"), meta: { title: "语音对话" } },
  { name: "media", path: "/media", children: [{ name: "gallery", path: "gallery", component: () => import("@/views/Gallery.vue"), meta: { transitionName: "slide-left", title: "画廊" } }] },
  {
    name: "article",
    path: "/article",
    children: [
      { name: "article-list", path: "", component: () => import("@/views/article/List.vue"), meta: { transitionName: "slide-left", title: "科学快讯" } },
      { name: "article-detail", path: "detail/:id", component: () => import("@/views/article/Detail.vue"), meta: { transitionName: "slide-left", title: "快讯详情" } },
    ],
  },
  { name: "about", path: "/about", component: () => import("@/views/shows/About.vue"), meta: { transitionName: "slide-left", title: "关于我们" } },
  { name: "FAQ", path: "/faq", component: () => import("@/views/FAQ.vue"), meta: { transitionName: "slide-left", title: "常见问题" } },
  { name: "prompts", path: "/prompts", component: () => import("@/views/Prompts.vue"), meta: { transitionName: "slide-left", title: "prompts" } },
  { name: "explanation", path: "/explanation", component: () => import("@/components/Explanation.vue"), meta: { transitionName: "slide-left", title: "使用手册" } },
  { name: "log", path: "/log", component: () => import("@/components/Log.vue"), meta: { transitionName: "slide-left", title: "更新日志" } },
  { name: "explanation", path: "/explanation", component: () => import("@/components/Explanation.vue"), meta: { transitionName: "slide-left", title: "使用手册" } },
  { name: "user-agreement", path: "/user-agreement", component: () => import("@/views/UserAgreement.vue"), meta: { transitionName: "slide-left", title: "用户协议" } },
  { name: "login", path: "/login", component: () => import("@/views/user/Login.vue"), meta: { transitionName: "slide-left", title: "登录" } },
];

//  创建路由实例并传递 `routes` 配置
const router = createRouter({
  // 4. 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
  // history: createWebHistory(),
  history: createWebHashHistory(),
  routes, // `routes: routes` 的缩写
});
// //beforeEach是router的钩子函数，在进入路由前执行
router.beforeEach((to, from, next) => {
  console.log(from);
  //判断是否有标题
  if (to?.meta?.title) {
    setDocumentTitle((to.meta.title + " | " + Site.slogan) as string);
  }
  appNotify.send(NotifyType.routerChange, NotifyOption.Updated, to.path);
  appContext.system.data.viewWidth < 1000 ? (appRef.isOpen = false) : (appRef.isOpen = true);
  next(); //执行进入路由，如果不写就不会进入目标页
});

export default router;
// 现在，应用已经启动了！
