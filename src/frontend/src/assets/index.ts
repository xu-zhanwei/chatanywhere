import message from './icons/chat/message.svg'
import onlineSearch from './icons/chat/online-search.svg'
import draw from './icons/chat/draw.svg'
import gallery from './icons/chat/gallery.svg'
import docmentSearch from './icons/chat/docment-search.svg'
import mindmap from './icons/chat/mindmap.svg'
import newsFlash from './icons/chat/news-flash.svg'

export default {
  message,
  onlineSearch, draw, gallery, docmentSearch, mindmap, newsFlash,
}