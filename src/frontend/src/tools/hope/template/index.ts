import { App, Component } from "vue";
import HLogin from "./login/h-login.vue";

const components: {
  [propName: string]: Component; //字面量类型，每个属性值类型为组件的类型
} = {
  HLogin,
};

/** 全局注册自定义组件 */
export default function (app: App<Element>) {
  for (const componentItme in components) {
    app.component(componentItme, components[componentItme]);
  }
}
