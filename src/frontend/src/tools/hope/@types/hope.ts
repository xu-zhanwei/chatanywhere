import { createVNode, render, VNodeChild } from "vue";
import hUpdatePopup from "../template/UpdatePopup.vue";
import hLoginVue from "../template/login/h-login.vue";
import hUpload from "@/components/asset/Upload.vue";
import { AssetInfo } from "@/models/asset";

interface HopeInterface {
  /** 全局统一登录 */
  login(): void;
  /** oss 快速上传 */
  upload(option: HopeNamespace.UploadOptions): void;
  /** 打开站点更新提示弹窗 */
  updatePopup(option: HopeNamespace.UpdatePopup): void;
  /** 销毁页面指定 id dom元素 */
  destroyDiv(name: string): void;
}

export class Hope implements HopeInterface {
  private static _ins = new Hope();
  private name: string = "hope-";

  public static get ins(): Hope {
    return Hope._ins ? Hope._ins : (Hope._ins = new Hope());
  }

  /**
   * 渲染
   * @param className class 类名
   * @param div 当前dom
   * @param template 模板
   * @param props 参数
   */
  private render(className: string, div?: HTMLDivElement | HTMLAudioElement, template?: any, props?: any) {
    const doms = document.getElementsByClassName(className);
    for (let i = 0; i < doms.length; i++) {
      doms[i].remove();
    }
    if (div) {
      div.setAttribute("class", className); // 为dom添加一个唯一标识类（无实质功能）
      document.body.appendChild(div); // 容器追加到body中
      const vnode = createVNode(template, props); // 将组件编译为虚拟dom节点
      render(vnode, div); // 将虚拟dom添加到div容器中
    }
  }
  /**
   * 网页统一登录
   */
  login(): void {
    const div = document.createElement("div");
    this.render(this.name + "login", div, hLoginVue, {});
  }
  /**
   * oss 快速上传
   */
  upload(option: HopeNamespace.UploadOptions): void {
    const div = document.createElement("div");
    this.render(this.name + "login", div, hUpload, option);
  }
  /**
   * 站点更新弹窗
   */
  updatePopup(option: HopeNamespace.UpdatePopup): void {
    const div = document.createElement("div");
    this.render(this.name + "updatePopup", div, hUpdatePopup, option);
  }

  /** 销毁页面指定 id dom元素 */
  destroyDiv(name: string): void {
    const div = document.createElement("div");
    this.render(this.name + name, div);
  }
}

export namespace HopeNamespace {
  export interface UpdatePopup {
    /** 点击取消回调 */
    cancel?: () => void;
    /** 点击确认回调 */
    confirm?: () => void;
  }
  export interface UploadOptions {
    /** 上传的文件列表 */
    files: File[];
    /** 上传成功回调 */
    success?: (assets: AssetInfo[]) => void;
    /** 上传错误回调 */
    fail?: (msg: string) => void;
    /** 完成回调，成功错误都会返回 */
    complete?: () => void;
  }

  export interface PreviewImageOptions {
    /** current 为当前显示图片的链接 */
    current: string | number;
    /** 需要预览的图片链接列表  */
    images: string[];
    /** 关闭时触发  */
    close?: () => void;
    /** 预览图片切换时触发 */
    indexChange?: (index: number) => void;
  }
  export interface MessageOptions {
    /** 类型  默认 success  */
    type?: "create" | "error" | "info" | "loading" | "success" | "warning";
    /** 所有 Message 是否显示 close 图标  默认 false */
    closable?: boolean;
    /** 弹窗显示毫秒数  默认 2000  */
    duration?: number;
    /** 弹窗显示文字  */
    content: string;
  }
  export interface NotificationOptions {
    /** 类型  默认 success  */
    type?: "create" | "error" | "info" | "success" | "warning";
    /** 所有 Message 是否显示 close 图标  默认 false */
    closable?: boolean;
    /** 弹窗显示毫秒数  默认 3000  */
    duration?: number;
    /** 操作区域的内容,可以是 render 函数  */
    action?: string;
    /** 头像区域的内容  */
    avatar?: () => VNodeChild;
    /** 标题  */
    meta?: string;
    /** 通知框内容，可以是 render 函数  */
    content: string;
    /** 描述的内容，可以是 render 函数  */
    description?: string;
    /** 当鼠标移入时是否保持通知框显示 false */
    keepAliveOnHover?: boolean;
  }

  export interface AudioContext {
    /** 音频的地址  */
    src: string;
  }
  export interface WxLoginOptions {
    /** 自定义回传参数 */
    // state?: WxOptionType;
  }
}
/** Hope 实例 */
export const hope = Hope.ins;
