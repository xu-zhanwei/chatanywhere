from fastapi import APIRouter
from .down import down_router
from .up import up_router

router = APIRouter(prefix="/synchronous", tags=["Synchronous"])

router.include_router(down_router)
router.include_router(up_router)
