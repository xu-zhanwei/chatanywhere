from fastapi import APIRouter

from .sms import sms_router

router = APIRouter(prefix="/common", tags=["Common"])

router.include_router(sms_router)
