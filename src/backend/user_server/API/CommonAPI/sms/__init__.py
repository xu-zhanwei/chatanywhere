from fastapi import APIRouter

from .send import send_router
from .verify import verify_router

sms_router = APIRouter(prefix="/sms")

sms_router.include_router(send_router)
sms_router.include_router(verify_router)
