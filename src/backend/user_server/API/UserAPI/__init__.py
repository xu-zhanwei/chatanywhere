from fastapi import APIRouter

from .login import login_router
from .logoff import logoff_router
from .password import password_router
from .register import register_router
from .token import token_router
from .update import update_router
from .get import get_router

router = APIRouter(prefix="/user", tags=["User"])
router.include_router(login_router)
router.include_router(password_router)
router.include_router(register_router)
router.include_router(token_router)
router.include_router(update_router)
router.include_router(logoff_router)
router.include_router(get_router)
