from fastapi import APIRouter, Header

from DataModels.database import User
from DataModels.share import RespInfo, RespResultCode, StatusInfo
from utils.token import check_token

logoff_router = APIRouter(prefix="/logoff")


@logoff_router.post("")
async def logoff(token=Header(None)):
    ok, info = check_token(token)
    if not ok:
        return info
    token = info
    phone = token.phone  # type: ignore
    user = await User.find_one(User.phone == phone)
    if not user:
        return RespInfo(
            status=StatusInfo(
                code=RespResultCode.PhoneNotFound,
                msg="未找到手机号",
            )
        )
    user.phone = ""
    await user.save()  # type: ignore
    return RespInfo(
        status=StatusInfo(
            code=RespResultCode.Success,
            msg="注销成功",
        ),
    )
