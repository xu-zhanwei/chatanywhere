from fastapi import APIRouter
from DataModels.database import User
from DataModels.share import RespInfo, RespResultCode, StatusInfo
from utils.user import user_to_user_info

get_router = APIRouter(prefix="/get")


@get_router.get("")
async def get_user_info(uid: str):
    user = await User.find_one(User.uid == uid)
    if user is None:
        return RespInfo(
            status=StatusInfo(
                code=RespResultCode.PhoneNotFound,
                msg="用户不存在",
            )
        )
    user_info = user_to_user_info(user)
    return RespInfo(
        status=StatusInfo(code=RespResultCode.Success, msg="获取成功"), info=user_info
    )
