from fastapi import APIRouter
from pydantic import BaseModel
from DataModels.share import RespInfo, RespResultCode, StatusInfo, AssetStatus
from DataModels.database import Asset
from utils.asset import asset_to_info


status_router = APIRouter(prefix="/status")


class StatusRequest(BaseModel):
    id: str
    status: AssetStatus


@status_router.post("")
async def update_status(data: StatusRequest):
    asset = await Asset.find_one(Asset.id == data.id)
    if not asset:
        return RespInfo(
            status=StatusInfo(
                code=RespResultCode.AssetNotFound,
                msg="资产不存在",
            )
        )
    asset.status = data.status
    await asset.save()  # type: ignore
    return RespInfo(
        status=StatusInfo(
            code=RespResultCode.Success,
            msg="更新成功",
        ),
        info=asset_to_info(asset),
    )
