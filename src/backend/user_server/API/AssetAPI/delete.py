from fastapi import APIRouter
from pydantic import BaseModel

from DataModels.database import Asset
from DataModels.share import RespInfo, RespResultCode, StatusInfo
from config import bucket_manager, qn_bucket

delete_router = APIRouter(prefix="/delete")


class DeleteRequest(BaseModel):
    id: str


@delete_router.post("")
async def delete_object(data: DeleteRequest):
    """

    |字段|说明|
    | --- | --- |
    |id| str （对象id）|

    这个端点不光会将记录从数据库中删除，还会远程删除对象存储中的文件
    """
    oid = data.id
    asset = await Asset.find_one(Asset.id == oid)
    if not asset:
        return RespInfo(
            status=StatusInfo(
                code=RespResultCode.AssetNotFound,
                msg="资产不存在",
            )
        )
    # asset.id = "del_" + asset.id
    # await asset.save()  # type: ignore
    bucket_manager.delete(qn_bucket, asset.url)
    await asset.delete()  # type: ignore

    return RespInfo(
        status=StatusInfo(
            code=RespResultCode.Success,
            msg="删除成功",
        )
    )
