from fastapi import APIRouter
from config import qn_down_url, qn_clinet
from DataModels.share import RespInfo, RespResultCode, StatusInfo


down_url_router = APIRouter(prefix="/down_url")


@down_url_router.get("")
async def get_down_url(key: str):
    """
    这个端点是为将来预留，如果将来把对象存储空间设置为私有，
    下载文件需要服务器签发的token，此端点会将签发好的token直接
    组合进 URL 中， 以供调用者直接下载。

    查询参数： key (在对象存储中的文件名)

    如何提取返回值：
    ```
    result = resp.json()
    down_url = result["info"]["down_url"]

    ```

    """
    private_down_url = qn_clinet.private_download_url(url=f"{qn_down_url}/{key}")

    return RespInfo(
        info={"down_url": private_down_url},
        status=StatusInfo(code=RespResultCode.Success, msg="获取成功"),
    )
