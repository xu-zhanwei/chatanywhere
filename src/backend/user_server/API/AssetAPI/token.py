from fastapi import APIRouter

from DataModels.share import (
    RespInfo,
    RespResultCode,
    StatusInfo,
    RegionEnum,
    AssetToken,
)
from config import qn_clinet, qn_expire, qn_bucket, qn_down_url

token_router = APIRouter(prefix="/token")


@token_router.get("")
async def get_up_token():
    f"""
    用于将文件上传到 OSS 的授权 token， 有效期 {qn_expire} 秒
    
    """
    up_token = qn_clinet.upload_token(
        bucket=qn_bucket,
        expires=qn_expire,
    )
    return RespInfo(
        status=StatusInfo(
            code=RespResultCode.Success,
            msg="获取成功",
        ),
        info=AssetToken(
            token=up_token, region=RegionEnum.z2, bucket=qn_bucket, domain=qn_down_url
        ),
    )
