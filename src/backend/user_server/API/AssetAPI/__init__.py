from fastapi import APIRouter

from .add import add_router
from .delete import delete_router
from .get import get_router
from .list import list_router
from .token import token_router
from .update import update_router
from .down_url import down_url_router
from .status import status_router
from .clean import clean_router

router = APIRouter(prefix="/asset", tags=["Asset"])

router.include_router(add_router)
router.include_router(delete_router)
router.include_router(get_router)
router.include_router(list_router)
router.include_router(update_router)
router.include_router(token_router)
router.include_router(down_url_router)
router.include_router(status_router)
router.include_router(clean_router)
