from fastapi import APIRouter
from pydantic import BaseModel

from DataModels.database import Asset
from DataModels.share import RespInfo, RespResultCode, StatusInfo

from utils.asset import asset_to_info

list_router = APIRouter(prefix="/list")


class ListRequest(BaseModel):
    keys: list[str]


@list_router.post("")
async def list_asset(data: ListRequest):
    """
    传入一个名为 keys 的列表，里面包含若干个对象id

    返回一个list， 包含对应的对象信息

    PS： 如果某个对象id不存在，则会报错。发生这种情况时，
    请通过 /asset/get 端点，遍历对象id列表，作为替代。

    报错信息中，会指明不存在的对象id，也可据此排除错误。
    """
    results = []
    for id in data.keys:
        asset = await Asset.find_one(Asset.id == id)
        if not asset:
            return RespInfo(
                status=StatusInfo(
                    code=RespResultCode.AssetNotFound,
                    msg=f"资产不存在:{id}",
                )
            )
        results.append(asset_to_info(asset))

    return RespInfo(
        status=StatusInfo(
            code=RespResultCode.Success,
            msg="获取成功",
        ),
        list=results,
    )
