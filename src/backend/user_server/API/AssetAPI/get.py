from fastapi import APIRouter

from DataModels.database import Asset
from DataModels.share import RespInfo, RespResultCode, StatusInfo
from utils.asset import asset_to_info

get_router = APIRouter(prefix="/get")


@get_router.get("")
async def get_object(id: str) -> RespInfo:
    """
    根据对象id返回对象信息
    """
    asset = await Asset.find_one(Asset.id == id)
    if not asset:
        return RespInfo(
            status=StatusInfo(
                code=RespResultCode.AssetNotFound,
                msg="资产不存在",
            )
        )
    return RespInfo(
        status=StatusInfo(
            code=RespResultCode.Success,
            msg="获取成功",
        ),
        info=asset_to_info(asset),
    )
