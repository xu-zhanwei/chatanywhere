"""
推荐配置信息全部从环境变量中读取

在Linux上，直接编辑 ~/.bashrc 或对应的配置文件， 再 source 即可

在 Windows上， 如果用 PowerShell，可以使用以下命令：
notepad $PROFILE # 打开配置文件

$env:OPENAI_API_KEY="sk-xxx" # 依此类推

完成编辑后， 输入
. $PROFILE # 重新加载配置

"""

import logging
import os

import qiniu
from motor.motor_asyncio import AsyncIOMotorClient

# Configure logging
logging.basicConfig(
    level=logging.DEBUG, format="%(asctime)s - %(levelname)s - %(message)s"
)

# Create a logger object
logger = logging.getLogger(__name__)

# MongoDB
client = AsyncIOMotorClient("mongodb://localhost:27017")

# QINIU
QINIU_SK = os.environ.get("QINIU_SK", "")
QINIU_AK = os.environ.get("QINIU_AK", "")
qn_bucket = "zaiwen-oss"
qn_clinet = qiniu.Auth(QINIU_AK, QINIU_SK)
bucket_manager = qiniu.BucketManager(qn_clinet)
qn_expire = 3600 * 8

qn_up_url = "https://up-z2.qiniup.com"
qn_down_url = "https://oss2.zaiwen.top"

# for JWT
SECRET = os.environ.get("SECRET", "ZSI6IjEyMzQ1Njc4OTAxIn01NiIsInR5cCI6IkpXVCJ9")

# TTC: Tencent Cloud
TCC_SECRET_ID = os.environ.get("TCC_SECRET_ID", "")
TCC_SECRET_KEY = os.environ.get("TCC_SECRET_KEY", "")
