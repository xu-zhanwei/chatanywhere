#
import random

from tencentcloud.common import credential
from tencentcloud.common.exception.tencent_cloud_sdk_exception import (
    TencentCloudSDKException,
)
from tencentcloud.common.profile.client_profile import ClientProfile
from tencentcloud.common.profile.http_profile import HttpProfile
from tencentcloud.sms.v20210111 import sms_client, models

from config import TCC_SECRET_ID, TCC_SECRET_KEY, logger
from typing import List


def random_code():
    return str(random.randint(1000, 9999))


def send_code(phone_number: str, params: List[str], template_id: str = "1954529"):
    """
    使用腾讯云短信服务向指定电话号码发送验证码。

    参数:
        phone_number (str): 要发送验证码的电话号码。
        code (str): 要发送的验证码。
        template_id (str): 要使用的消息模板的ID（默认为"1954529"）。
    """
    try:
        cred = credential.Credential(TCC_SECRET_ID, TCC_SECRET_KEY)

        http_profile = HttpProfile()
        http_profile.endpoint = "sms.tencentcloudapi.com"

        client_profile = ClientProfile()
        client_profile.httpProfile = http_profile
        client = sms_client.SmsClient(cred, "ap-beijing", client_profile)

        req = models.SendSmsRequest()
        req.PhoneNumberSet = [phone_number]
        req.SmsSdkAppId = "1400861847"
        req.SignName = "北京在问科技有限公司"
        req.TemplateId = template_id
        req.TemplateParamSet = params

        resp = client.SendSms(req)
        return resp.to_json_string()

    except TencentCloudSDKException as err:
        logger.error(err)
        return err


if __name__ == "__main__":
    pass
