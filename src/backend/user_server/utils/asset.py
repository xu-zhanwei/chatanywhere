from DataModels.database import Asset
from DataModels.share import AssetInfo

from config import qn_down_url


def asset_to_info(asset: Asset) -> AssetInfo:
    return AssetInfo(
        id=asset.id,
        name=asset.name,
        remark=asset.remark,
        md5=asset.md5,
        format=asset.format,
        size=asset.size,
        url=f"{qn_down_url}/{asset.url}",
        small=f"{qn_down_url}/{asset.small}",
        snapshot=f"{qn_down_url}/{asset.snapshot}",
        owner=asset.owner,
        created=asset.created,
        width=asset.width,
        height=asset.height,
        status=asset.status,
    )
