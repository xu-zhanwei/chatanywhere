import bcrypt


def hashpwd(pwd: str):
    return bcrypt.hashpw(pwd.encode('utf-8'), bcrypt.gensalt())


def checkpwd(pwd: str, hashpwd: bytes):
    return bcrypt.checkpw(pwd.encode('utf-8'), hashpwd)
