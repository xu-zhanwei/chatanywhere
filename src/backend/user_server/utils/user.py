from DataModels.database import User
from DataModels.user import UserInfo


def user_to_user_info(user: User) -> UserInfo:
    return UserInfo(
        uid=user.uid,
        nick=user.nick,
        sex=user.sex,
        avatar=user.avatar,
        birthday=user.birthday,
        phone=user.phone,
        created=user.created,
        openid=user.openid,
    )
