from utils.pwd import hashpwd, checkpwd


def test_right_hash():
    pwd = "123456"
    hash = hashpwd(pwd)
    assert checkpwd(pwd, hash)


def wrong_hash():
    pwd = "123456"
    hash = hashpwd(pwd)
    wrong_pwd = "654321"
    assert not checkpwd(wrong_pwd, hash)
