import time

from utils.share import timestamp


def test_timestamp():
    assert int(time.time()) == timestamp()
