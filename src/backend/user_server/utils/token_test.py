from DataModels.share import RespResultCode, TokenPayload
from utils.share import timestamp
from utils.token import generate_token, decode_token, check_token


def test_generate_decode_token():
    tp = TokenPayload(
        phone="12345678901",
        createTime=timestamp(),
    )
    t = generate_token(tp)

    d_t = decode_token(t)
    assert d_t.phone == tp.phone
    assert d_t.createTime == tp.createTime


def test_check_token_expired():
    tp = TokenPayload(
        phone="12345678901",
        createTime=timestamp() - (60 * 60 * 24 * 7 + 1),
    )
    t = generate_token(tp)

    ok, info = check_token(t)
    assert ok is False
    assert info.status.code == RespResultCode.TokenExpire  # type: ignore


def test_check_token_wrong_token():
    tp = TokenPayload(
        phone="12345678901",
        createTime=timestamp(),
    )
    t = generate_token(tp) + "wrong"

    ok, info = check_token(t)
    assert ok is False
    assert info.status.code == RespResultCode.DataCorrupted  # type: ignore


def test_check_token_no_enough_info():
    tp = TokenPayload(
        phone="",
        createTime=timestamp(),
    )

    t = generate_token(tp)

    ok, info = check_token(t)
    assert ok is False
    assert info.status.code == RespResultCode.ExtractPhoneErr  # type: ignore


def test_check_token_no_token():
    ok, info = check_token("")
    assert ok is False
    assert info.status.code == RespResultCode.TokenEmpty  # type: ignore
