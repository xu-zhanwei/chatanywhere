## 〇、 如何运行

填写配置文件（`config.py`）后， `python main.py` 即可启动服务。

推荐将配置文件全写入环境变量中。


## 一、 Tips

### 0. 如何在本地访问VPS的 `Mongodb` 数据库

用 ssh 隧道
```shell
ssh -N -L 27017:localhost:27017 root@120.48.3.22
```

### 1. 为已有数据模型添加字段
**以为`User`模型添加`openid`字段为例子**

打开`mongoshell`，执行如下命令:
```mongosh
db.User.updateMany({ "openid": { $exists: false } }, { $set: { "openid": "" } })
```


### 2. `tools` 文件夹提供了若干工具脚本