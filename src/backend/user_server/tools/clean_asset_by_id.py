import httpx
import asyncio


sem = asyncio.Semaphore(10)
lock = asyncio.Lock()
client = httpx.AsyncClient(
    headers={
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.142.86 Safari/537.36",
        "Content-Type": "application/json",
        "Accept": "application/json",
    }
)
old_asset_ids = """\
c6e5f2e0-9a53-451f-91bf-1e1f698f55f6
8269491b-56d1-4124-a517-403388975e73
20d4bff9-6fee-4403-9a52-e0910a9c01e6
13bbee14-29e3-4a82-a4fc-4f25290b68d7
04829060-d69a-4301-b4d4-3b62a580e75d
"""


async def ada(
    id: str, index: int, sem: asyncio.Semaphore = sem, lock: asyncio.Lock = lock
):
    async with sem:
        url = "https://user.chatcns.com/asset/delete"
        try:
            response = await client.post(url, json={"id": id})
            st_code = response.json().get("status").get("code")
            print(st_code, ":::", id, ":::", index)
        except Exception as e:
            print(e)
            async with lock:
                with open("error.txt", "a", encoding="utf8") as f:
                    f.write(id + "\n")


async def main():
    tasks = []
    # with open("all_id.txt", "r", encoding="utf8") as f:
    for index, line in enumerate(old_asset_ids.split("\n")):
        line = line.strip()
        if line:
            tasks.append(ada(line, index))

    await asyncio.gather(*tasks)


if __name__ == "__main__":
    asyncio.run(main())
