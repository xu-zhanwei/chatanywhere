"""
激活虚拟环境后，使用以下命令运行：

```sh
# 因为需要依赖主项目中的数据模型，所以
# 脚本需要以 python 模块的方式运行
python -m tools.how_many_old_assets
```

结果如下：

```text
当前共有 $n 个 asset(s) 符合以下条件： 
    - 创建时间超过7天
    - 所属者 为 'anonymous'
```

"""


from DataModels.database import Asset
from motor.motor_asyncio import AsyncIOMotorClient
from beanie import init_beanie
import asyncio
import time


async def init():
    # MongoDB
    client = AsyncIOMotorClient("mongodb://localhost:27017")

    await init_beanie(
        database=client.Users,
        document_models=[Asset],
    )


async def main():
    await init()

    ids = []
    old = await Asset.find(
        Asset.created < (int(time.time()) - 7 * 24 * 60 * 60),
        Asset.owner == "anonymous",
        # limit=10000,
    ).to_list()

    for asset in old:
        ids.append(asset.id)
    print(
        f"当前共有 {len(ids)} 个 asset(s) 符合以下条件： \n\t- 创建时间超过7天\n\t- 所属者 为 'anonymous'"
    )
    # print(old[-1])


if __name__ == "__main__":
    asyncio.run(main())
