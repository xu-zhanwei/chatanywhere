import typing

from beanie import Document

from .user import SexEnum
from .share import KeyValue, AssetStatus


class User(Document):
    uid: str  # = str(uuid.uuid4())
    nick: str  # = f"User{str(uuid.uuid4())[:5]}"
    sex: SexEnum = SexEnum.WoMan
    avatar: str = ""
    birthday: int = 0
    code: str
    verified: bool = False
    phone: str
    created: int
    password: str = ""
    openid: str = ""


class Asset(Document):
    id: typing.Optional[str]
    name: typing.Optional[str]  # file name
    remark: typing.Optional[str]
    md5: typing.Optional[str]
    format: typing.Optional[str]
    size: typing.Optional[int]
    url: typing.Optional[str]  # uuid 图像、文件 对应七牛上面的文件名
    small: typing.Optional[str]  # 图像特有， 略缩图
    snapshot: typing.Optional[str]  # 图像特有， 压缩后的封面快照
    owner: typing.Optional[str]  # 文件拥有者的uid，或者匿名游客
    created: int
    width: typing.Optional[int]
    height: typing.Optional[int]
    status: AssetStatus


class Sync(Document):
    uid: str
    created: int
    data: typing.List[KeyValue]
