from enum import Enum
from typing import Optional

from pydantic import BaseModel


class SexEnum(Enum):
    WoMan = 0
    Man = 1


class UserInfo(BaseModel):
    uid: str
    nick: str
    sex: SexEnum
    avatar: Optional[str]
    birthday: Optional[int]
    phone: Optional[str]
    created: int
    openid: Optional[str]
