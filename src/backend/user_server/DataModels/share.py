from enum import Enum
from typing import Optional, Any, List

from pydantic import BaseModel


class TokenPayload(BaseModel):
    phone: str
    createTime: int


class RespResultCode(Enum):
    Success = 0

    # 其他错误码，需要时再添加
    TokenEmpty = 2
    TokenExpire = 3
    TokenMiss = 4
    SendCodeErr = 11
    CodeErr = 13
    CodeExpire = 14
    DataCorrupted = 15  # 解密加密code失败
    ExtractPhoneErr = 16  # 解密后， 从中提取手机号码失败
    PhoneExist = 21
    PhoneNotFound = 22  # 未找到手机号，用户不存在
    UnkownLoginType = 31  # 未知登录类型
    AssetAddFail = 41  # 资产添加失败
    AssetNotFound = 42  # 资产不存在
    SyncStoreFail = 51  # 同步记录存储失败
    SyncNotFound = 52  # 同步记录不存在
    TemplateNotExist = 61  # 模板不存在
    ParamsNotEnough = 71  # 参数不足


class StatusInfo(BaseModel):
    code: RespResultCode
    msg: Optional[str]


# 公共返回体
class RespInfo(BaseModel):
    info: Optional[Any] = None
    list: Optional[Any] = None
    total: Optional[int] = None
    pages: Optional[int] = None
    token: Optional[str] = None
    code: Optional[str] = None
    status: StatusInfo


class AssetStatus(Enum):
    Defaule = 0
    Wicked = 1
    Public = 2


class AssetInfo(BaseModel):
    id: Optional[str]
    name: Optional[str]
    remark: Optional[str]
    md5: Optional[str]
    format: Optional[str]
    size: Optional[int]
    url: Optional[str]
    small: Optional[str]
    snapshot: Optional[str]
    owner: Optional[str]
    created: int
    width: Optional[int]
    height: Optional[int]
    status: AssetStatus


class RegionEnum(Enum):
    z0 = "z0"
    z1 = "z1"
    z2 = "z2"
    na0 = "na0"
    as0 = "as0"
    cnEast2 = "cn-east-2"


class AssetToken(BaseModel):
    token: str
    region: RegionEnum
    bucket: str
    domain: str  # 被 CDN 加速的域名


class KeyValue(BaseModel):
    key: str
    value: str


class SMSTemplateEnum(Enum):
    Verify = "1954529"
    SecretKey = "2074119"


class SMSConfig(BaseModel):
    phone: str
    params: Optional[List[str]] = None
    template: Optional[SMSTemplateEnum] = SMSTemplateEnum.Verify
