from typing import Any, Dict, TypeVar, Mapping
import os 
import yaml
from pydantic import BaseModel
import pymongo as mg
from pymongo.database import Database
class CoreSetting(BaseModel):
    host: str = '127.0.0.1'
    port: int = 8100
    reload: bool = True
    debug: bool = True

settingFolder = os.path.dirname(os.path.abspath(__file__))
settingPath = os.path.join(settingFolder,"setting.yaml")
settingDict = yaml.safe_load(open(settingPath,'r'))
setting = CoreSetting(**settingDict)

class DatabaseInfo(BaseModel):
    dialect:str
    driver:str = ""
    host:str
    port:int
    username:str = ""
    password:str = ""
    database:str = "" 
    def url(self):
        if(self.dialect=="mongodb"):
            return f"{self.dialect}://{self.host}:{self.port}"
dbInfo:Dict[str,DatabaseInfo] = {
    "mongodb":DatabaseInfo(**{
        "dialect":"mongodb",
        "host":"localhost",
        "port":27017
    })
}

mgClient:Dict[str,Database] = mg.MongoClient(dbInfo["mongodb"].url())
