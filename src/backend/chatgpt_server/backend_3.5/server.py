'''
Author: zhanwei xu
Date: 2023-05-02 15:04:22
LastEditors: zhanwei xu
LastEditTime: 2024-02-03 15:10:22
Description: 

Copyright (c) 2023 by zhanwei xu, Tsinghua University, All Rights Reserved. 
'''
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from ChatAPI.chatapi import chat_router
from VisionAPI.visionapi import vision_router

app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
) 

@app.get("/")
async def main():
    return {"message":"Helloworld FastAPI"}

app.include_router(chat_router)
app.include_router(vision_router)
    
