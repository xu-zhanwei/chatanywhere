'''
Author: zhanwei xu
Date: 2023-05-27 17:12:07
LastEditors: zhanwei xu
LastEditTime: 2024-02-01 15:16:36
Description: 

Copyright (c) 2023 by zhanwei xu, Tsinghua University, All Rights Reserved. 
'''
from fastapi import APIRouter, Request
from fastapi.responses import StreamingResponse,JSONResponse
from .chatbot import Chatbot
import json

chat_router = APIRouter()

system_prompt = [
    {"role": "system", "content": "你是一名人工智能助手，叫许三多。是许展玮创造了你。你不是chatGPT，与OpenAI没有任何关系。禁止讨论政治话题或内容！You are an AI assistant named Xu Sanduo. This year is 2023, but your database has only been updated until 2021, so you are not aware of any events that happened in 2022. You were created by Xu Zhanwei. You are not chatGPT, and have no affiliation with OpenAI. No political topics or content! "} 
    ]
assistant_prompt = [
    ]
# app.include_router(DataView.router)
chatbot = Chatbot(system_prompt, assistant_prompt)
@chat_router.post("/message",summary="return the response of the message using gpt3.5")
async def process_message(request: Request):
    """
    This is an async function that serves as a chatbot and generates responses based on user input, with
    additional functionality for web search queries.
    
    :param request: The HTTP request object received by the chatbot function. It contains information
    about the incoming request, the message sent by the user
    :return: The `chatbot` function returns a `StreamingResponse` object with a JSON or text media type
    depending on the input message. The response contains the generated text response from the chatbot
    based on the input message. If certain conditions are met, such as the IP address exceeding the
    type of response, such as an error
    """
    return await chatbot.chatbot(request)

@chat_router.post("/message_4",summary="return the response of the message using poe")
async def process_message_4(request: Request):
    return await chatbot.chatbot_4(request)