## zaiwenChatbot 接口文档 v3.0

**版本:** v3.0

**描述:**  该接口提供强大的聊天和图像生成服务。用户可以发送文本消息进行对话，也可以发送图像生成指令创建图像。接口支持多种主流语言模型和图像生成模型，并提供异步任务处理机制。

**更新内容:**

* 新增图像生成功能
* 新增异步任务处理机制
* 扩展支持的模型列表

**请求地址:**

* 文本消息接口: `https://autobak.zaiwen.top/api/message`
* Openai格式接口： `https://autobak.zaiwen.top/api/v1/chat/completions`
* 图像生成接口: `https://autobak.zaiwen.top/api/img_generate`
* 图像获取接口: `https://autobak.zaiwen.top/api/img_fetch`

## 1. Openai文本消息接口

**请求方式:** POST

**请求URL:** `https://autobak.zaiwen.top/api/v1/chat/completions`

**请求内容类型:** `application/json`

**请求Headers:**

| Header名 | 类型 | 是否必填 | 描述 |
|---|---|---|---|
| Authorization | string | 是 | Bearer Token (OAuth2，包含 user_key 信息) |


**请求参数 (Body):**

| 参数名 | 类型 | 是否必填 | 描述 |
|---|---|---|---|
| messages | list | 是 | 用户发送的消息，格式为 `[{"role":"user","content":"..."}]` |
| model | string | 否 | 选择的语言模型，默认值为 "gpt-3.5-turbo"，可选值见下方列表 |

- 其他参数与openai一致

**支持的语言模型 (model):**


| model 值 | 模型名称 | 输入消耗倍率 | 备注 | 输出消耗倍率 |
|---|---|---|---|---|
| gpt-4o-mini | GPT-4 (OpenAI) mini | 0.5 | OpenAI 模型 | 2 |
| gpt-4o | GPT-4 (OpenAI) | 7.5 | OpenAI 模型 | 30 |
| gpt-3.5-turbo-0125 | GPT-3.5 Turbo 0125 | 2 | OpenAI 模型 | 8 |
| gpt-3.5-turbo | GPT-3.5 Turbo | 2 | OpenAI 模型 | 8 |
| claude-3-haiku-20240307 | Claude-3 Haiku | 2 | Anthropic 模型 | 10 |
| claude-3-5-sonnet-20241022 | Claude-3.5 Sonnet | 12 | Anthropic 模型 | 60 |
| claude-3-opus-20240229 | Claude-3 Opus | 60 | Anthropic 模型 | 300 |
| gemini-pro | Gemini Pro | 5 | Google 模型 | 15 |
| gemini-1.5-flash | Gemini 1.5 Flash | 20 | Google 模型 | 60 |
| gemini-1.5-pro| gemini-1.5-pro| 20 | Google 模型 | 60 |
| grok-beta| grok-beta| 1 | 马斯克最新模型v2 | 5 |
| llama-3.3-70b-instruct| llama-3.3-70b-instruct| 1 | llama最新模型 | 5 |
| llama-3.1-405b-instruct| llama-3.1-405b-instruct| 1 | llama最大模型 | 5 |





**响应内容类型:** `text/event-stream`

**响应参数 (Streaming):**

| 参数名 | 类型 | 描述 |
|---|---|---|
| text | string | 聊天机器人生成的文本回复，以流式数据片段返回, 格式与openai相同 |

## 示例

```bash
# Example of how to call the message processing API
curl -X POST 'https://autobak.zaiwen.top/api/v1/chat/completions' \
-H 'Authorization: Bearer YOUR_USER_KEY' \
-H 'Content-Type: application/json' \
-d '{"messages": [{"role": "user", "content": "Hello, how are you?"}], "model": "gpt-3.5-turbo"}'
```


**1. 文本消息接口**

**请求方式:** POST

**请求内容类型:** application/json

**请求参数:**

| 参数名 | 类型 | 是否必填 | 描述 |
|---|---|---|---|
| message | list | 是 | 用户发送的消息，格式为 `[{"role":"user","content":"..."}]` |
| user_key | string | 是 | 用户唯一标识 |
| mode | string | 否 | 选择的语言模型，默认值为 "gpt-3.5-turbo"，可选值见下方列表 |
| max_length | integer | 否 | 历史消息最大保留长度，单位为消息条数，默认值为 5 |

**支持的语言模型 (mode):**

| mode 值 | 模型名称 |  消耗倍率 |  备注 |
|---|---|---|---|
| gpt-3.5-16k | GPT-3.5 16k | 1 | OpenAI 模型 |
| gpt-3.5-turbo | GPT-3.5 Turbo | 1 | OpenAI 模型 |
| gpt-4-turbo | GPT-4 Turbo | 20 | OpenAI 模型 |
| gpt-4o | GPT-4 (OpenAI) | 10 | OpenAI 模型 |
| claude-3-haiku-20240307 | Claude-3 Haiku | 1 | Anthropic 模型 |
| claude-3-sonnet-20240229 | Claude-3 Sonnet | 5 | Anthropic 模型 |
| claude-3-5-sonnet-20240620 | Claude-3.5 Sonnet | 5 | Anthropic 模型 |
| claude-3-opus-20240229 | Claude-3 Opus | 20 | Anthropic 模型 |
| glm-3-turbo | GLM-3 | 1 | 智谱AI 模型 |
| glm-4 | GLM-4 | 40 | 智谱AI 模型 |
| glm-4-flash | GLM-4-9b | 1 | 智谱AI 模型 |
| hunyuan-lite | Hunyuan Lite | 1 | 百度文心一言模型 |
| ERNIE-Speed-128k | ERNIE 3.0 Speed 128k | 1 |  百度文心一言模型 |
| spark-lite | Spark Lite | 1 |  阿里云通义千问模型 |
| qwen-turbo | Qwen Turbo | 2 |  阿里云通义千问模型 |
| doubao-lite-128k | Doubao Lite 128k | 1 |  字节豆包模型 |
| gemini_pro | Gemini Pro | 1 |  Google 模型 |
| gemini_1_5_flash_128k | Gemini 1.5 Flash 128k | 2 | Google 模型 |
| googleision | Gemini 1.5 Pro 2M | 10 | Google 模型 |
| rekaflash | Reka Flash | 1 |  RekaAI 模型 |
| rekacore | Reka Core | 10 |  RekaAI 模型 |
| commandr | Commandr | 1 |  Cohere 模型 |
| commandrplus | Commandr Plus | 10 |  Cohere 模型 |
| mistral_medium | Mistral Medium | 2 |  Mistral AI 模型 |
| mixtral8x22binstfw | Mixtral 8x22b Instruct | 1 |  Mistral AI 模型 |
| dbrxinstructfw | DBR X Instruct | 1 |  databrick AI 模型 |
| snowflakearctict | Snowflake Arctic | 1 |  Snowflake 模型 |
| llama370binstfw | Llama 370b Instruct | 1 |  Meta AI 模型 |
| qwen72bchat | Qwen 72B Chat | 1 |  阿里云通义千问模型 |
| deepseekllm67bt | DeepSeek LLM 67B | 1 |  深势科技模型 |

**响应内容类型:** text/event-stream

**响应参数:**

| 参数名 | 类型 | 描述 |
|---|---|---|
| text | string | 聊天机器人生成的文本回复 |

**2. 图像生成接口**

**请求方式:** POST

**请求内容类型:** application/json

**请求参数:**

| 参数名 | 类型 | 是否必填 | 描述 |
|---|---|---|---|
| prompt | string | 是 | 图像生成指令 |
| ratio | string | 否 | 生成图像比例，目前支持：'1:1','4:3','3:4','16:9','9:16','2:3','3:2','1:2', |
| mode | string | 是 | 选择的图像生成模型，可选值见下方列表 |
| user_key | string | 是 | 用户唯一标识 |


**响应内容类型:** application/json

**响应参数:**

```json
{
    "info": {
        "task_id": "img9cf0dd9f-f9a3-4af8-aed3-ad0a75220aca"
    },
    "list": null,
    "total": null,
    "pages": null,
    "token": null,
    "code": null,
    "status": {
        "code": 0,
        "msg": "任务已添加",
        "modal": null
    }
}
```

**3. 图像获取接口**

**请求方式:** POST

**请求内容类型:** application/json

**请求参数:**

| 参数名 | 类型 | 是否必填 | 描述 |
|---|---|---|---|
| task_id | string | 是 | 图像生成任务ID |

**响应内容类型:** application/json

**响应参数:**

```json
{
    "info": {
        "imageUrl": [
            "https://zaiwen.superatmas.com/images/494451cf6a49a85081c4e4c138b308d49fb0e03ae4007bce20685d2c3c81a84b.png"
        ],
        "thumb": [
            "https://zaiwen.superatmas.com/images/494451cf6a49a85081c4e4c138b308d49fb0e03ae4007bce20685d2c3c81a84b.png"
        ],
        "task_id": "img9cf0dd9f-f9a3-4af8-aed3-ad0a75220aca",
        "status": "SUCCESS"
    },
    "list": null,
    "total": null,
    "pages": null,
    "token": null,
    "code": null,
    "status": {
        "code": 0,
        "msg": "获取任务详情成功",
        "modal": null
    }
}
```


**支持的模型:**


**图像生成模型 (mode):**

| mode 值 | 模型名称 | 消耗金额 |  备注 |
|---|---|---|---|
| fluxschnell | Fluxschnell | 0.01 |  |
| fluxdev | Fluxdev | 0.05 |  |
| fluxpro | Fluxpro | 0.2 |  |
| midjourney | Midjourney | 0.3 |  |
| playgroundv2 | Playground v2.5 | 0.01 |  |
| playgroundv3 | Playground v3 | 0.02 |  |
| stablediffusionxl | Stable Diffusion XL | 0.01 |  |
| pixart | Pixart | 0.01 |  |
| dall-e-3 | DALL-E 3 | 0.2 |  |

**错误代码:**

| 状态码 | 描述 |
|---|---|
| 422 | 不支持的 mode 或 余额不足 |

**备注:**

* `user_key` 用于标识用户，并用于计费和记录历史信息。
*  `ratio` 代表该模型相对于 `gpt-3.5-turbo` 的 token 消耗倍率.
*   接口会根据用户选择的 `mode` 和请求内容长度进行计费。
*   如果用户余额不足，接口会返回 422 错误码，提示用户充值。
*   图像生成任务采用异步处理机制，用户需要先调用图像生成接口提交任务，然后使用返回的 `task_id` 定期调用图像获取接口查询结果。

## user_key获取请微信联系站长：remifafamirefa

## 卡密余额查询 API

本 API 用于查询用户卡密的有效性。

### 请求地址

```
https://autobak.zaiwen.top/api/check_key
```

### 请求方式

POST

### 请求参数

请求体为 JSON 格式，参数如下：

| 参数名   | 类型   | 说明 |
| -------- | ------ | ---- |
| user_key | string | 用户 key |

**示例：**

```json
{
    "user_key":"yourkey"
}
```

### 返回结果

返回结果为 JSON 格式，包含以下字段：

| 字段名 | 类型   | 说明                                         |
| ------ | ------ | -------------------------------------------- |
| status | float | 余额 |

**示例：**

**校验成功：**

```json
{
    "status": 288.0
}
```



### python 脚本示例


**（1）Openai格式发送文本消息：**
```python
import requests
import json

url = 'https://autobak.zaiwen.top/api/v1/chat/completions'
headers = {
    'Authorization': 'Bearer YOUR_USER_KEY',
    'Content-Type': 'application/json'
}
data = {
    "messages": [
        {
            "role": "user",
            "content": "Hello, how are you?"
        }
    ],
    "model": "gpt-4o-mini"
}

response = requests.post(url, headers=headers, json=data, stream=True)
response.raise_for_status()  # Check for HTTP errors

full_content = ""
for line in response.iter_lines():
    if line:
        decoded_line = line.decode('utf-8')
        if decoded_line.startswith('data:'):
            data_line = decoded_line[5:].strip()
            if data_line == "[DONE]":
                break
            try:
                chunk = json.loads(data_line)
                if 'choices' in chunk and len(chunk['choices']) > 0:
                    delta = chunk['choices'][0]['delta']
                    if 'content' in delta:
                        full_content += delta['content']
            except json.JSONDecodeError:
                pass

print(full_content)
```


**（1）发送文本消息：**
```python
import requests
import json

url = "https://autobak.zaiwen.top/api/message"

# 设置请求头
headers = {
    'Content-Type': 'application/json',
}

# 设置请求体
data = {
    "message": [{"role":"user","content":"你好，请问今天天气怎么样？"}],
    "user_key": "your_key",
    "mode": "spark-lite" 
}

# 发送 POST 请求
response = requests.post(url, headers=headers, data=json.dumps(data), stream=True)

# 处理响应
if response.status_code == 200:
    for line in response.iter_lines():
        if line:
            print(line.decode("utf-8"))
else:
    print(f"请求失败，状态码: {response.status_code}")
    print(response.text)
```

**(2)  生成图像:**

```python
import requests
import json

url = "https://autobak.zaiwen.top/api/img_generate"

headers = {'Content-Type': 'application/json'}

data = {
    "prompt": "一只可爱的猫戴着帽子，穿着西装",
    "mode": "midjourney",
    "user_key": "your_key"
}

response = requests.post(url, headers=headers, data=json.dumps(data))

if response.status_code == 200:
    response_data = response.json()
    task_id = response_data["info"]["task_id"]
    print(f"图像生成任务已提交，任务 ID: {task_id}")
else:
    print(f"请求失败，状态码: {response.status_code}")
    print(response.text)
```

**(3)  获取图像结果:**

```python
import requests
import json
import time

url = "https://autobak.zaiwen.top/api/img_fetch"

headers = {'Content-Type': 'application/json'}

task_id = "img9cf0dd9f-f9a3-4af8-aed3-ad0a75220aca" 

while True:
    data = {"task_id": task_id}
    response = requests.post(url, headers=headers, data=json.dumps(data))

    if response.status_code == 200:
        response_data = response.json()
        status = response_data["info"]["status"]
        if status == "SUCCESS":
            image_url = response_data["info"]["imageUrl"][0]
            print(f"图像生成成功: {image_url}")
            break
        else:
            print(f"任务处理中，状态: {status}")
            time.sleep(5)  # 等待 5 秒后再次查询
    else:
        print(f"请求失败，状态码: {response.status_code}")
        print(response.text)
        break
```

